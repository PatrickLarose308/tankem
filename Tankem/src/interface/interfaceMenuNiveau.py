# coding: utf-8

from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import sys



class InterfaceMenuNiveau(ShowBase):
    def __init__(self, daoNiveau, daoBalance, dtoBalance):
        
        self.daoBalance = daoBalance
        self.daoNiveau = daoNiveau
        self.dtoBalance = dtoBalance


        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        # Image d'arrière plan
        self.background = OnscreenImage(parent = render2d, image = "../asset/Menu/menuPrincipal.jpg") 
        self.message = "Choisir un niveau"
        

        self.controlTextScale = 0.10
        self.controlBorderWidth = (0.005, 0.005)

        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)

        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")


        
        self.getMapsInfos()
        
        #Titre du jeu
        self.textTitre = OnscreenText(text = self.message,
                                      pos = (0,0.75), 
                                      scale = 0.25,
                                      fg=(0.8,0.9,0.7,1),
                                      align=TextNode.ACenter)

        verticalOffsetControlButton = 0.225
        verticalOffsetCenterControlButton = -0.02
        self.myScrolledListLabel = DirectScrolledList(
                decButton_pos = (0.0, 0.0, verticalOffsetControlButton + verticalOffsetCenterControlButton),
                decButton_text = "Monter",
                decButton_text_scale = 0.08,
                decButton_borderWidth = (0.0025, 0.0025),
                decButton_frameSize = (-0.35, 0.35, -0.0375, 0.075),
                decButton_text_fg = (0.15, 0.15, 0.75, 1.0),

                incButton_pos = (0.0, 0.0, -0.625 - verticalOffsetControlButton + verticalOffsetCenterControlButton),
                incButton_text = "Descendre",
                incButton_text_scale = 0.08,
                incButton_borderWidth = (0.0025, 0.0025),
                incButton_frameSize = (-0.35, 0.35, -0.0375, 0.075),
                incButton_text_fg = (0.15, 0.15, 0.75, 1.0),

                pos = (0, 0, 0.3),

                items = self.scrollItemButtons,
                numItemsVisible = 5,
                forceHeight = 0.175,
                
                frameSize = (-1.05, 1.05, -0.95, 0.325),
                frameColor = (0.5, 0.5, 0.5, 0.75),

                itemFrame_pos = (0.0, 0.0, 0.0),
                itemFrame_frameSize = (-1.025, 1.025, -0.775, 0.15),
                itemFrame_frameColor = (0.35, 0.35, 0.35, 0.75),
                itemFrame_relief = 1
            )

        self.quitButton = DirectButton(
                text = ("Quitter", "Quitter", "Quitter", "disabled"),
                text_scale = self.controlTextScale,
                borderWidth = self.controlBorderWidth,
                relief = 2,
                pos = (0.0, 0.0, -0.75),
                frameSize = (-0.5, 0.5, -0.0625, 0.105),
                command = lambda : sys.exit(),
            )

    def createItemButton(self, mapName, mapId):
        return DirectButton(
                text = mapName,
                text_scale = self.controlTextScale, 
                borderWidth = self.controlBorderWidth, 
                relief = 2,
                frameSize = (-1.0, 1.0, -0.0625, 0.105),
                command = lambda: self.loadGame(mapId))

    def createAllItems(self, mapsInfo):
        scrollItemButtons = [self.createItemButton(u'-> Carte aléatoire <-', None)]
        for mapInfo in mapsInfo:
            scrollItemButtons.append(self.createItemButton(self.formatText(mapInfo[1]), mapInfo[0]))
        return scrollItemButtons

    def formatText(self, text, maxLength = 20):
        return text if len(text) <= maxLength else text[0:maxLength] + '...'

    def loadGame(self, mapId):
        # À ajuster... évidemment...
        if mapId:
            self.daoNiveau.getConnection()
            self.daoNiveau.selectMap(int(mapId))
            self.daoNiveau.closeConnection()

            self.daoBalance.getConnection()
            self.daoBalance.select(self.daoNiveau.dto.selectedMap.balanceId)
            self.dtoBalance = self.daoBalance.dto
            self.daoBalance.closeConnection()

            Sequence(Func(lambda : self.transition.irisOut(0.2)),
                     SoundInterval(self.sound),
                     Func(self.cacher),
                     Func(lambda : messenger.send("DemarrerPartie")),
                     Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                     Func(lambda : self.transition.irisIn(0.2))
            ).start()
        else:
            Sequence(Func(lambda : self.transition.irisOut(0.2)),
                     SoundInterval(self.sound),
                     Func(self.cacher),
                     Func(lambda : messenger.send("DemarrerPartie")),
                     Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                     Func(lambda : self.transition.irisIn(0.2))
            ).start()


    def getMapsInfos(self):
        if self.daoNiveau.getConnection():
            self.daoNiveau.selectMapInfo()
            self.scrollItemButtons = self.createAllItems(self.daoNiveau.dto.mapsInfo)
            self.daoNiveau.closeConnection()
        else:
            self.message = "Erreur de connexion à la BD"
            self.scrollItemButtons = [self.createItemButton(u'-> Carte aléatoire <-', None)]


    def cacher(self):
            #Est esssentiellement un code de "loading"

            #On remet la caméra comme avant
            base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
            #On cache les menus
            self.background.hide()
            self.myScrolledListLabel.hide()
            self.textTitre.hide()
            self.quitButton.hide()
	

    # Ne pas oublier qu'il y a des ajustements à faire dans la fonction 
    # 'cacher' de Tankem et à d'autres endroits.

