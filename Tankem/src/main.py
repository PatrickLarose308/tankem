## -*- coding: utf-8 -*-
#Ajout des chemins vers les librairies
from util import inclureCheminCegep
import sys
import os
import inspect

#Importe la configuration de notre jeu
from panda3d.core import loadPrcFile
loadPrcFile("config/ConfigTankem.prc")

#Module de Panda3DappendObject
from direct.showbase.ShowBase import ShowBase

#Modules internes
from gameLogic import GameLogic
from interface import InterfaceMenuPrincipal
from interface import InterfaceMenuNiveau

#Modules DAO
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
from common.internal.dao.sanitizerBalanceDTO import SanitizerBalanceDTO
from common.internal.dao.balanceDAOoracle import BalanceDAOoracle
from common.internal.daoLevelEditor.levelEditorDAOoracle import LevelEditorDAOoracle


class Tankem(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.dto = None
        self.getDTO()
        self.daoNiveau = LevelEditorDAOoracle()
        self.demarrer(self.dto)
        self.accept("ConstruireInterfaceNiveau",self.buildInterfaceMenu)

    def demarrer(self, dto):
        self.gameLogic = GameLogic(self, self.dto, self.daoNiveau.dto)
        #Commenter/décommenter la ligne de votre choix pour démarrer le jeu
        #Démarre dans le menu
        self.menuPrincipal = InterfaceMenuPrincipal()
        #Démarre directement dans le jeu
        #messenger.send("DemarrerPartie")
        

    def getDTO(self):
        self.daoBalance = BalanceDAOoracle()
        if self.daoBalance.getConnection():
            self.daoBalance.select()
            self.dto = self.daoBalance.dto
            self.daoBalance.closeConnection()
        else:
            self.dto = self.daoBalance.sanitizer.initializeDTOBalanceWithDefaultValues()
            self.dto.messageAccueil = "Problème de connexion. Configuration par défaut utilisée."

    def buildInterfaceMenu(self):
        self.menuNiveau = InterfaceMenuNiveau(self.daoNiveau, self.daoBalance, self.dto)

#Main de l'application.. assez simple!
app = Tankem()
app.run()