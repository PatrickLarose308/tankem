<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();
?>
<html lang="en"></html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/index.css">
        <script src="js/indexAnimation.js"></script>
        <script src="js/sprite/Tile.js"></script>
        <script src="js/sprite/Cursor.js"></script> 
        <script src="js/sprite/Player.js"></script>                 
        <script src="js/jquery.js"></script>    
        <title>Tankem Level Editor</title>
    </head>
    <body>
        <h1>Tankem: Éditeur de carte</h1>
        <div id="container">
            <canvas id="canvas"></canvas>
        </div>
        <div id="panel">
            <div class="w3-bar w3-black">
                <button class="w3-bar-item w3-button" onclick="openTab('parametres')">Parametres</button>
                <button class="w3-bar-item w3-button" onclick="openTab('balance');displayBalance()">Balance</button>
                <button class="w3-bar-item w3-button" onclick="openTab('controls')">Contrôles</button>
            </div>
            <div id="parametres" class="tab" >
                <div>
                    <h2 id="name"></h2>
                </div>
                <div id="dimensions">
                    <h4>Dimensions:</h4>
                    <div>
                        X:<input type="number"  min="6" max="12" id="x" value="6" oninput="changeX()" onkeydown="return false">
                    </div>
                    <div>
                        Y:<input type="number"  min="6" max="12" id="y" value="6" oninput="changeY()" onkeydown="return false">
                    </div>
                </div>
                <div id="dimensions">
                    <h4>Délai d'apparition des items (secondes):</h4>
                    <div>
                        Min:<input type="number" id="min" value="3" min="0" oninput="changeMin()">
                    </div>
                    <div>
                        Max:<input type="number" id="max" value="20" min="0" oninput="changeMax()">
                    </div>
                </div>
                <div id="status">
                    <div>
                        <h4>Status: </h4>
                        <select id="drop_down" onchange="setStatus()">
                            <!-- <option value="actif">Actif</option>
                            <option value="test">Test</option>
                            <option value="inactif">Inactif</option> -->
                        </select>
                    </div>
                </div>
                <div id="players">
                    <h4>Joueurs:</h4>
                    <button onclick="setPlayer1()">Joueur1</button>
                    <button onclick="setPlayer2()">Joueur2</button>                    
                </div>
                <div>
                    <h4>Types de murs:</h4>
                </div>
                <div id="type_mur">
                    <button onclick="setWallType(types.plancher)" class="btn">Plancher</button>
                    <button onclick="setWallType(types.mur_fixe)" class="btn">Mur fixe<img src="images/fence.png" alt="Mur fixe"></button>
                    <button onclick="setWallType(types.mur_vertical)" class="btn">Mur vertical<img src="images/arrowup.png" alt="Mur vertical"></button>
                    <button onclick="setWallType(types.mur_inverse)" class="btn">Mur vertical inverse<img src="images/arrowdown.png" alt="Mur vertical inversé"></button>                                                            
                    <button onclick="setTree(types.mur_inverse)" class="btn">Arbre<img src="images/tree.png" alt="Arbre"></button>                                                                                
                </div>

                <div id="options">
                    <button onclick="saveMap()">Sauvegarder la carte</button> 
                    <button onclick="createMap()">Réinitialiser la carte</button>                     
                </div>
                <h4>Feedback:</h4>
                <div id="chat"></div>
            </div>
            <div id="balance" class="tab" style="display:none">
                <div>
                    <div>
                        <h4>Choisir une balance: </h4>
                        <select id="drop_down_balance" onchange="getBalance()"></select>
                    </div>
                    <div id="balance_list"></div>
                </div>
            </div>
            <div id="controls" class="tab" style="display:none">
                <h4>Contrôles:</h4>
                <div>A: Gauche</div>
                <div>W: Haut</div>
                <div>D: Droite</div>
                <div>S: Bas</div>
                <div>Espace: ajouter/enlever arbre</div>
                <div>m/k: Changer le type de mur</div>
                <div>1: ajouter/enlever le joueur 1</div>
                <div>2: ajouter/enlever le joueur 2</div>
                <div>Vous pouvez utiliser la souris pour sélectionner un tuile.</div>
            </div>
        </div>
    </body>
</html>