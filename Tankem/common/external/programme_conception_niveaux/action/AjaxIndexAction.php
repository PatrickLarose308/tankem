<?php
	require_once("action/CommonAction.php");
	require_once("action/LevelEditorDAO/LevelEditorDAOOracle.php");
	require_once("action/LevelEditorDAO/Tuile.php");	

	class AjaxIndexAction extends CommonAction {
		public $result = "";
		public $dao = null;
        
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
			$this->dao = new LevelEditorDAOOracle();
		}

		protected function executeAction() {
			if (isset($_POST["saveMap"])) {
				// Remplir le DTO avec les valeur entrées par l'usager
				$map = json_decode($_POST['infos'], true);
				$this->dao->dto->nom = $map["nom"];
				$this->dao->dto->status = $map["status"];
				$this->dao->dto->delaiMin = str_replace(".",",",(string)$map["delaiMin"]);
				$this->dao->dto->delaiMax = str_replace(".",",",(string)$map["delaiMax"]);
				$this->dao->dto->nbTuilesX = $map["nb_tuiles_x"];
				$this->dao->dto->nbTuilesY = $map["nb_tuiles_y"];
				$this->dao->dto->balance = $map["balance"];
				foreach ($map["tuiles"] as $tile) {
					$this->dao->dto->tuiles[] = new Tuile($tile["nom"], $tile["joueur"], $tile["arbre"], $tile["x"], $tile["y"]);
				}

				// Insertion des données dans la base de données
				$this->result = $this->dao->insert();
			}

			if (isset($_POST["balance"])) {
				$this->result = $this->dao->selectBalance($_POST["balance"]);
			}

			if (isset($_POST["balance_id"])) {
				$this->result = $this->dao->selectIdBalance();
			}
		}
	}
	