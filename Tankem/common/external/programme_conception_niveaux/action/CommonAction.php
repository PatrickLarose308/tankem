<?php
	session_start();
	require_once("action/LevelEditorDAO/LevelEditorSanitizerDTO.php");

	abstract class CommonAction {
		public static $VISIBILITY_PUBLIC = 0;
		public static $VISIBILITY_MEMBER = 1;
		public static $VISIBILITY_MODERATOR = 2;
		public static $VISIBILITY_ADMINISTRATOR = 3;

		private $pageVisibility;
		private $sanitizer;
		protected $dto;

		public function __construct($pageVisibility) {
			$this->pageVisibility = $pageVisibility;
			$this->sanitizer = new LevelEditorSanitizerDTO();
			$this->dto = $this->sanitizer->initializeDTO();
		}

		public function execute() {
			if (!isset($_SESSION["visibility"])) {
				$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
			}

			if ($_SESSION["visibility"] < $this->pageVisibility) {
				$this->quit();
			}

			$this->executeAction();
		}

		private function quit() {
			$this->pageVisibility = CommonAction::$VISIBILITY_PUBLIC;
			session_unset();
			session_destroy();
			session_start();
		}
		
		abstract protected function executeAction();
	}