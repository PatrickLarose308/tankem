<?php
	class LevelEditorFilter {
		public static function checkType($elem, $type) {
			$result = false;

			if (gettype($elem) == $type) {
				$result = true;
			}

			return $result;
		}

		public static function checkRange($elem, $type) {
			$result = false;

			$range = 9999999999;
			if ($type == "string") {
				$elem = strlen($elem);
				$range = 20;
			}
			if ($elem >= 0 && $elem <= $range) {
				$result = true;
			}
			
			return $result;
		}
	}	
