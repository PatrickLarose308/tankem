<?php
	class Tuile {
		public $nom;
		public $joueur;
		public $arbre;
		public $x;
		public $y;

		public function __construct($nom, $joueur, $arbre, $x, $y) {
			$this->nom = $nom;
			$this->joueur = $joueur;
			$this->arbre = $arbre;
			$this->x = $x;
			$this->y = $y;
		}
	}	
