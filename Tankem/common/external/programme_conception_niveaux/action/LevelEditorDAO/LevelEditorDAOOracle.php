<?php
	require_once("action/LevelEditorDAO/LevelEditorSanitizerDTO.php");
	require_once("action/LevelEditorDAO/LevelEditorDTO.php");
	require_once("action/LevelEditorDAO/Connection.php");

	class LevelEditorDAOOracle {

		public function __construct() {
			$this->sanitizer = new LevelEditorSanitizerDTO();
			$this->dto = $this->sanitizer->initializeDTO();
		}

		public function insert() {
			$result = "OK";

			// Nettoyer les données
			$error = $this->sanitizer->sanitizeDTO($this->dto);
			if (count($error) > 0) {
				$result = $error;
			}
			else {
				try {

					$connection = Connection::getConnection();
	
					// Vérification du nom
					$trouve = false;
					$requete = "SELECT nom FROM niveau WHERE nom = '" . $this->dto->nom . "'";
					foreach($connection->query($requete) as $rep) {
						$trouve = true;
						$result = "NOM_EXISTANT";
					}
	
					if (!$trouve) {
	
						// Insertion du niveau dans la base de données
						$requete = "CALL pkg_tankem_level_editor.proc_insertion_niveau(?, ?, ?, ?, ?, ?, ?, ?)";
						$cursor = $connection->prepare($requete);
						$cursor->bindParam("1",$this->dto->status, PDO::PARAM_STR, 20);
						$cursor->bindParam("2",$this->dto->balance, PDO::PARAM_INT, 9999999999);
						$cursor->bindParam("3",$this->dto->nom, PDO::PARAM_STR, 20);
						$date = date("Y-m-d");
						$cursor->bindParam("4",$date, PDO::PARAM_STR, 10);
						$cursor->bindParam("5",$this->dto->delaiMin, PDO::PARAM_STR, 10);
						$cursor->bindParam("6",$this->dto->delaiMax, PDO::PARAM_STR, 10);
						$cursor->bindParam("7",$this->dto->nbTuilesX, PDO::PARAM_INT, 9999999999);
						$cursor->bindParam("8",$this->dto->nbTuilesY, PDO::PARAM_INT, 9999999999);
						$cursor->execute();
		
						// Insertion des tuiles
						$requete = "CALL pkg_tankem_level_editor.proc_insertion_tuile(?, ?, ?, ?, ?, ?)";
						foreach ($this->dto->tuiles as $tuile) {
							$cursor = $connection->prepare($requete);
							$cursor->bindParam("1",$tuile->nom, PDO::PARAM_STR, 20);
							$cursor->bindParam("2",$this->dto->nom, PDO::PARAM_STR, 20);
							$cursor->bindParam("3",$tuile->x, PDO::PARAM_INT, 12);
							$cursor->bindParam("4",$tuile->y, PDO::PARAM_INT, 12);
							$cursor->bindParam("5",$tuile->joueur, PDO::PARAM_INT, 9999999999);
							$cursor->bindParam("6",$tuile->arbre, PDO::PARAM_INT, 1);
							$cursor->execute();
						}
					}
				}
				catch(Exception $e) {
					$result = "ERREUR_INSERTION";
				}
			}
			return $result;
		}

		public function selectBalance($id) {
			$result = [];

			// Aller chercher les parametres caractere pour cette balance
			$connection = Connection::getConnection();
			$requete = "SELECT c.valeur, p.nom  FROM ParametreCaractere c, NomParametre p WHERE c.balance = ? AND p.id = c.nom_id" ;
			$query = $connection->prepare($requete);
			$query->bindParam(1, $id, PDO::PARAM_INT);
			$query->execute();
			$result[] = $query->fetchall(PDO::FETCH_ASSOC);

			// Aller chercher les parametres numeriques pour cette balance
			$requete = "SELECT n.valeur, p.nom  FROM ParametreNumerique n, NomParametre p WHERE n.balance = ? AND p.id = n.nom_id AND n.categorie = 0" ;
			$query = $connection->prepare($requete);
			$query->bindParam(1, $id, PDO::PARAM_INT);
			$query->execute();
			$result[] = $query->fetchall(PDO::FETCH_ASSOC);

			return $result;
		}

		public function selectIdBalance() {
			$result = [];

			// Aller chercher tous les id de balance disponibles
			$connection = Connection::getConnection();
			$requete = "SELECT id FROM Balance";
			foreach($connection->query($requete) as $rep) {
				$result[] = $rep["ID"];
			}
			return $result;
		}
	}	