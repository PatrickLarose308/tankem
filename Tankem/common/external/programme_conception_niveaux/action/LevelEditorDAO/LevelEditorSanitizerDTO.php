<?php
	require_once("action/LevelEditorDAO/LevelEditorDTO.php");
	require_once("action/LevelEditorDAO/LevelEditorFilter.php");

	class LevelEditorSanitizerDTO {
		public function __construct() {
			$this->defaultValues = [];
			$this->defaultValues["status"] = ["", "string"];
			$this->defaultValues["balance"] = [0, "integer"];
			$this->defaultValues["tuiles"] = [[], "array"];
			$this->defaultValues["nom"] = ["", "string"];
			$this->defaultValues["delaiMin"] = [0, "string"];
			$this->defaultValues["delaiMax"] = [0, "string"];
			$this->defaultValues["nbTuilesX"] = [0, "integer"];
			$this->defaultValues["nbTuilesY"] = [0, "integer"];

			$this->tuileDefaultValues["nom"] = ["", "string"];
			$this->tuileDefaultValues["joueur"] = [0, "integer"];
			$this->tuileDefaultValues["arbre"] = [false, "boolean"];
			$this->tuileDefaultValues["x"] = [0, "integer"];
			$this->tuileDefaultValues["y"] = [0, "integer"];
		}

		public function initializeDTO() {
			$dto = new LevelEditorDTO();
			foreach ($this->defaultValues as $key => $value) {
				$dto->$key = $value[0];
			}
			return $dto;
		}

		public function sanitizeAll($defaultValues, $values) {
			$error = [];
			
			foreach ($values as $key => $value) {
				// Vérification des tuiles
				if ($key == "tuiles") {
					foreach($value as $tuile) {
						$error_tuile = $this->sanitizeAll($this->tuileDefaultValues, get_object_vars($tuile));
						if (count($error_tuile) > 0) {
							$error[] = $error_tuile;
						}
						if ($tuile->arbre && $tuile->joueur) {
							$error[] = "Un joueur ne pas pas commencer sur une case avec un arbre";
						}
					}
				}
				else {

					// Vérification du type des variables
					$type = $defaultValues[$key][1];
					if (!LevelEditorFilter::checkType($value, $type)) {
						$error[] = "La variable \"" . $key . "\" n'est pas du type \"" . $type . "\"";
					}
					else {

						// Vérification de la grandeur des variables
						if (!LevelEditorFilter::checkRange($value, $type)) {
							$error[] = "La variable \"" . $key . "\" n'est pas entre le minimum et le maximum permis";
						}
					}
				}
			}
			return $error;
		}

		public function sanitizeDTO($dto) {
			return $this->sanitizeAll($this->defaultValues, $dto);
		}
	}	
