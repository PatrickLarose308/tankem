Utilisation de l'�diteur de niveau :

-Avant l'utilisation :

	1 - L'�diteur de niveau est une application web n�cessitant un serveur pour
	 pouvoir ex�cuter le code PHP. Au c�gep, d�poser le dossier 'programme_conception_niveaux'
	 dans htdocs.

-Utilisation :

	1 - Il est possible de modifier le niveau, c'est-�-dire le contenu des tuiles, avec
	    la souris et\ou avec le clavier.

	2 - Avec la souris :

		-Cliquez sur la case que vous voulez s�lectionner.
	 	-Pour assigner la position initiale du joueur 1, cliquez sur le bouton "Joueur1".
		 Sa position initiale sera la tuile qui �tait s�lectionn�e.
		-Pour changer le type d'une tuile, il faut la s�lectionner, puis appuyer sur le
		 bouton du type voulu, soit "Plancher", "Mur fixe", "Mur vertical" ou "Mur vertical inverse".
		-Vous pouvez ajouter un arbre sur n'importe quelle tuile, sauf sur une tuile
		 �tant la position initiale d'un joueur.

	3 - Avec le clavier :
		
		-D�placer vous sur le niveau avec les touches "W", "A", "S" et "D".
		-Pour assigner la position initiale du joueur 1, appuyer sur la touche "1".
		 Sa position initiale sera la tuile qui �tait s�lectionn�e. Pour assigner
		 la position initiale du joueur 2, appuyer sur la touche "2".
		-Pour changer le type d'une tuile, appuyer sur les touches "M" et "K" pour 
		 parcourir les types disponibles de la tuile s�lectionn�e.
		-Pour mettre un arbre sur la tuile s�lectionn�e, appuyer sur la touche "L"
		 Il ne faut pas mettre d'arbre sur la m�me tuile que la position initiale d'une joueur.

	4 - Param�tres :
		
		-Pour changer les dimensions du niveau, utiliser les bo�tes de dimensions. Des 
		 fl�ches apparaissent quand vous mettez la souris sur la bo�te.
		-Pour changer les d�lais d'apparition des items, �crivez directement dans le zones
		 indiqu�es. Le maximum ne peut �tre inf�rieur au minimum.
		-S�lectionner le status dans la bo�te indiqu�e.
		-Pour sauvegarder votre niveau, appuyez sur le bouton "Sauvegarder la carte".
		-Pour vider la carte, appuyer sur le bouton "R�initialiser la carte".

	5 - Balance :
	
		-Pour choisir la balance qui sera utilis�e par le niveau, aller dans l'onglet balance.
		 Une liste des balances existantes sera pr�sentes, indiquant tous les param�tres de 
		 la balance s�lectionn�e. � la sauvegarde, la balance s�lectionn�e sera envoy�e.

-R�sum� des touches :

	Souris : Plein de fonctionnalit�s
	W : Monter sur la carte
	A : Aller � gauche sur la carte
	S : Descendre sur la carte
	D : Aller � droite sur la carte
	M : Changer le type de la tuile actuelle
	K : Changer le type de la tuile actuelle
	L : Mettre\enlever un arbre
	1 : D�terminer la position initiale du joueur 1
	2 : D�terminer la position initiale du joueur 2


















		