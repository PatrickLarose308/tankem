/*Le curseur n'est pas qu'une simple boîte vide servant à indiquer à l'utilisateur où il 
se situe sur la grille. Elle fait également une bonne partie du travail. C'est ici que l'on 
accède aux différentes tuiles et que nous pouvons interragir avec.*/

class Cursor {
    constructor(x, y){
        this.x = x;
        this.y = y;
        this.positionX = this.x * tileWidth + tileOffset;
        this.positionY = this.y * tileWidth + tileOffset;
        
        //Le curseur ne reçoit pas de tuile en paramètres, mais il garde toujours comme attribut la tuile sur laquelle il pointe
        this.tile = null;
        this.setTile(this.x, this.y);
    }

    setCursor(x, y){
        if(!(this.x > xSize - 1 || this.x < 0 || this.y > ySize - 1 || this.y < 0)){
            this.x = x;
            this.y = y;
        }
        this.setTile(x, y);
    }

    //Le curseur ne reçoit pas de tuile en paramètres, mais il garde toujours comme attribut la tuile sur laquelle il pointe
    setTile(x, y){
        for (let i = 0; i < tileList.length; i++) {
            const tile = tileList[i];
            if(tile.x == this.x && tile.y == this.y){
                this.tile = tile;
            }
        }
    }

    setPlayer(player){
        if(this.tile instanceof Tile){
            if(this.tile.tree){
                feedBack("Un joueur ne peut pas être sur la même case qu'un arbre");
            } else {
                this.tile.setPlayer(player);
            }
        }
    }

    setWallType(type){
        if(this.tile instanceof Tile){
            this.tile.setWallType(type);
        }
    }
    
    setTree(){
        if(this.tile instanceof Tile){
            if(this.tile.player instanceof Player){
                feedBack("Un abre ne peut pas être sur la même case qu'un joeur");
            } else {
                this.tile.setTree();
            }
        }
    }

    tick(){
        if(leftPushed != null || rightPushed != null || upPushed != null || downPushed != null || lPushed != null){
            if(leftPushed){
                if(!(this.x - 1 < 0)){
                    this.x -= 1;
                }
                leftPushed = null;
            } else if(rightPushed){
                if(this.x + 1 < xSize){
                    this.x += 1; 
                }
                rightPushed = null;                
            } else if(upPushed){
                if(!(this.y - 1 < 0)){
                    this.y -= 1;
                }
                upPushed = null;
            } else if(downPushed){
                if(this.y + 1 < ySize){
                    this.y += 1;
                }
                downPushed = null;
            }

            if(lPushed){
                lPushed = null;
                this.setTree();
            }

            if(kPushed){
                kPushed = null;
                if(this.tile instanceof Tile){
                    this.tile.scrollTroughTypes(-1);
                }
            } else if(mPushed){
                mPushed = null;
                if(this.tile instanceof Tile){
                    this.tile.scrollTroughTypes(1);
                }
            }

            if(onePushed){
                onePushed = null;
                this.setPlayer(player1);
            } else if(twoPushed){
                twoPushed = null;
                this.setPlayer(player2);
            }

            this.setCursor(this.x, this.y);
        }

        //Si le curseur n'est sur aucune tuile elle est redessinée sur la tuile (5, 5);
        if(this.x > xSize - 1 || this.x < 0 || this.y > ySize - 1 || this.y < 0){
            this.x = 5;
            this.y = 5;
        }

        this.positionX = this.x * tileWidth + tileOffset;
        this.positionY = this.y * tileWidth + tileOffset;

        //Dessiner l'encadrement
        ctx.strokeStyle = "rgb(255,0,0)";
        ctx.lineWidth=4;
        ctx.beginPath()
        ctx.rect(this.positionX, this.positionY, tileWidth, tileWidth);
        ctx.stroke();
        ctx.closePath();
    }
}