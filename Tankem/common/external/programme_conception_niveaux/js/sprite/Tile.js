class Tile {
    constructor(x, y, type=types.plancher, tree=false, player=null){
        this.x = x;
        this.y = y;
        this.tree = tree;
        this.player = player;
        this.setPlayer(this.player);
        
        this.imgFloor = new Image();
        this.imgTree = new Image();
        
        this.type = null; 
        this.setWallType(type);
        this.alive = true;

        //C'est la proportion que l'image d'arbre va prendre par rapport à la case
        this.treeDimensions = 0.4;
        this.imgTree.src = "images/tree.png";

        this.positionX = this.x * tileWidth + tileOffset;
        this.positionY = this.y * tileWidth + tileOffset;
    }

    //Permet de savoir si la tuile est vide, pour l'envoie dans la liste. 
    isEmpty(){
        let empty = true;
        if(this.player instanceof Player){
            empty = false;
        }
        if(this.tree){
            empty = false;
        }
        if(this.type != types.plancher){
            empty = false;
        }
        return empty;
    }

    //Permet de faire défiler les types de tuiles
    scrollTroughTypes(moreOrLess){
        let i = 0;
        let newType = 0;
        for (let item in typesList) {
            if(typesList[i] == this.type){
                newType = i + moreOrLess;
                if(newType < 0){
                    newType = typesList.length -1;
                }
                if(newType > typesList.length -1){
                    newType = 0;
                }
            }
            i++;
        }
        setWallType(typesList[newType]);
    }

    //Le design est fait pour que la tuile possède le joeur et non le contraire. On passe aux tuiles
    //Les joueurs (1 et 2) et les tuiles se les approprient et en change les coordonnées, pas le contraire. 
    setPlayer(player){
        if(player instanceof Player){
            if(this.player instanceof Player){
                if(this.player != player){
                    feedBack("Les deux joueurs ne peuvent pas être sur la même case.");
                } else {
                    this.player.tile = null;
                    this.player = null;
                }
            } else {
                if(player.tile instanceof Tile){
                    player.tile.player = null;  //On enlève à l'ancienne tuile le joueur
                    player.tile = null;
                }
                this.player = player;
                this.player.tile = this;
            }
        }
    }

    die(){
        this.alive = false;
    }

    setWallType(type){
        if(type == types.mur_fixe){
            this.imgFloor.src = "images/fence.png";
            this.type = type;
        } else if(type == types.mur_vertical){
            this.imgFloor.src = "images/arrowup.png";
            this.type = type;
        } else if(type == types.mur_inverse){
            this.imgFloor.src = "images/arrowdown.png";
            this.type = type;
        } else {
            //Dans le cas du plancher vide, l'image n'est pas changé, elle n'est simplement pas dessinée. 
            this.type = types.plancher;
        }
    }

    setTree(){
        if(this.tree){
            this.tree = false;
        } else {
            this.tree = true;
        }
    }
    
    tick(){
        this.positionX = this.x * tileWidth + tileOffset;
        this.positionY = this.y * tileWidth + tileOffset;

        //On dessine le background
        ctx.fillStyle = "rgba(100, 100, 100, 0.8)";
        ctx.fillRect(this.positionX, this.positionY, tileWidth, tileWidth);
        
        //On dessine les arbres
        if(this.tree){
            let treeWidth = tileWidth * this.treeDimensions;
            let posX = this.positionX + tileWidth - treeWidth;
            let posY = this.positionY + tileWidth - treeWidth;
            ctx.drawImage(this.imgTree, posX, posY, treeWidth, treeWidth);
        }

        //On dessine l'image du plancher'
        if(this.type != types.plancher){
            ctx.drawImage(this.imgFloor, this.positionX, this.positionY, tileWidth, tileWidth);
        }

        //Dessiner l'encadrement
        ctx.strokeStyle = "rgba(0,0,0, 1.0)";
        ctx.lineWidth=2;
        ctx.beginPath()
        ctx.rect(this.positionX, this.positionY, tileWidth, tileWidth);
        ctx.stroke();
        ctx.closePath();

        //Si le joueur est définie, il sera dessinée sur la tuile
        if(this.player instanceof Player){
            ctx.fillStyle = "rgba(255, 255, 255, 1.0)";
            ctx.font = "30px Arial";
            ctx.fillText(this.player.number, this.positionX + tileOffset, this.positionY + tileWidth - tileOffset);
        }

        //Les tuiles se mettent à jour elles-mêmes, elles meurent si leurs coordonnées sont hors des dimensions saisies
        //Si le curseur n'est sur aucune tuile elle est redessinée sur la tuile (5, 5);
        if(this.x > xSize - 1 || this.x < 0 || this.y > ySize - 1 || this.y < 0){
            this.alive = false;
            if(this.player instanceof Player){
                //On indique au joueur qu'il n'a plus aucune tuile
                this.player.tile = null;
            }
        }
        return this.alive;
    }
}