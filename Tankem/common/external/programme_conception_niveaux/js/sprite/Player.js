class Player {
    constructor(number = null, tile = null){
        this.number = number;
        this.tile = null;
        this.setTile(tile);
    }

    isTileSetted(){
        if(this.tile instanceof Tile){
            return true;
        } else {
            return false;
        }
    }

    setTile(tile){
        //On attribue la nouvelle tuile
        if(tile instanceof Tile){
            //On efface l'ancienne tuile
            if(this.tile instanceof Tile){
                this.tile.player = null;
            }
            this.tile = tile;
            this.tile.player = this;
        }
    }
}