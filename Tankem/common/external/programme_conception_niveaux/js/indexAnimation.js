let types = Object.freeze({"plancher":"plancher", "mur_fixe":"mur_fixe", "mur_vertical":"mur_vertical", "mur_inverse":"mur_inverse"})
//On crée une liste contenant chacun des types pour pouvoir itérer avec des entiers
let typesList = [];
for (let item in types) {
    if (isNaN(Number(item))) {
        typesList.push(item);
    }
}

let statusTypes = Object.freeze({"Actif":"actif", "Test":"test", "Inactif":"inactif"}) //Les informations doivent correpsondre dans le #drop_down

let canvas = null;
let ctx = null;
let panel = null;
let tileList = [];
let tileWidth = null;
let canvasWidth = 900;

//Les variables de la logique
let name = "Title";
let xSize = 6;
let ySize = 6;
let min = 3;
let max = 20;
let status = statusTypes.Actif;

//Les widgets permettant de représenter et d'interragir avec les variables
let nameLabel = null;
let xSpinner = null;
let ySpinner = null;
let minSpinner = null;
let maxSpinner = null;
let dropDown = null;
let minMaxSpinnersMaximum = 100000000;


//Le curseur et les joueurs sont des éléments indépendants tout comme les tuiles
let cursor = null;
let player1 = null;
let player2 = null;

//Les touches pour contrôler le curseur et changer les types de tuiles
let rightPushed = false;            
let leftPushed = false;            
let upPushed = false;
let downPushed = false;
let lPushed = false;
let kPushed = false;
let mPushed = false;
let onePushed = false;
let twoPushed = false;

//Le numero des touches
left = 65;  
right = 68;
up = 87;
down = 83
l = 76;
k = 75;
m = 77;
one = 49;
two = 50;

//On met quelques pixels supplémentaires pour que l'affichage des tuiles soit uniforme
let mapExtraBorder = 4;
let tileOffset = mapExtraBorder/2;

//Largeur maximale du canevas
let canMaxWidth = 0.5;

//La console pour le feedback
let chat = null;

//Permet de gérer l'affichage de la balance
let balance_list = null;
let balanceDisplayed = false;
let drop_down_balance = null;
let balanceId = 1;

//Les éléments de l'application se redimensionnent lorsque l'on redimensionne le navigateur
window.onresize = calculateElementsDimensionsAndPositions;

window.onload = () => {
    canvas = document.getElementById("canvas");
    panel = document.getElementById("panel");
    ctx = canvas.getContext("2d");

    //Transmet au curseur ses nouvelles coordonnées en clicant sur une case;
    canvas.onclick = e => {
        let cnvDimensions = canvas.getBoundingClientRect();

        let clicX = e.clientX - cnvDimensions.left - tileOffset;
        let clicY = e.clientY - cnvDimensions.top - tileOffset;
        
        let cursorX = Math.floor(clicX/tileWidth);
        let cursorY = Math.floor(clicY/tileWidth);

        if(cursorX < 0){
            cursorX = 0;
        }
        if(cursorX >= 12){
            cursorX = 11;
        }
        if(cursorY < 0){
            cursorY = 0;
        }
        if(cursorY >= 12){
            cursorY = 11;
        }
        cursor.setCursor(cursorX, cursorY);
    }

    chat = document.getElementById("chat");
    balance_list = document.getElementById("balance_list");
    drop_down_balance = document.getElementById("drop_down_balance");

    initializeKeysListener();
    initializeMapInfos();
    calculateElementsDimensionsAndPositions();
    createMap();
    //On place les joueurs après les tuiles pour pouvoir attribuer les tuiles aux joueurs
    player1 = new Player(1);
    player2 = new Player(2);
    //On crée le curseur après les joueurs
    cursor = new Cursor(0,0);
    generalTick();
}

//Initialise les écouteurs pour les touches utilisés pour interragir avec l'éditeur
function initializeKeysListener(){
	document.onkeydown = function (e) {
		if (e.which == left) leftPushed = true; 
		else if (e.which == right) rightPushed = true;
        else if (e.which == up) upPushed = true;
        else if (e.which == down) downPushed = true;
        else if (e.which == l) lPushed = true;
        else if (e.which == k) kPushed = true;
        else if (e.which == m) mPushed = true;
        else if (e.which == one) onePushed = true;
        else if (e.which == two) twoPushed = true;
    }
	
	document.onkeyup = function (e) {
		if (e.which == left) leftPushed = false; 
		else if (e.which == right) rightPushed = false;
        else if (e.which == up) upPushed = false;
        else if (e.which == down) downPushed = false;
        else if (e.which == l) lPushed = false;
        else if (e.which == k) kPushed = false;
        else if (e.which == m) mPushed = false;
        else if (e.which == one) onePushed = false;
        else if (e.which == two) twoPushed = false;
    }
}

//Enlève les écouteurs des touches pour pouvoir écrire du texte
function removeKeysListener(){
	document.onkeydown = function (e) {
		if (e.which == left) leftPushed = false; 
		else if (e.which == right) rightPushed = false;
        else if (e.which == up) upPushed = false;
        else if (e.which == down) downPushed = false;
        else if (e.which == l) lPushed = false;
        else if (e.which == k) kPushed = false;
        else if (e.which == m) mPushed = false;
        else if (e.which == one) onePushed = false;
        else if (e.which == two) twoPushed = false;
	}
}

function initializeMapInfos(){
    nameLabel = document.getElementById("name");
    //Pour pouvoir changer le titre comme demandé
    $('#name').click(function(){
        let title = $(this).text();
        $(this).html('');
        $('<input></input>')
            .attr({
                'type': 'text',
                'name': 'fname',
                'id': 'txt_name',
                'size': '30',
                'value': title
            })
            .appendTo('#name');
        $('#txt_name').focus();
    });
    
    $(document).on('blur','#txt_name', function(){
        let title = $(this).val();
        if(title){
            $('#name').text(title);
            name = title;
        } else {
            $('#name').text(name);
        }         
    });

    xSpinner = document.getElementById("x");
    ySpinner = document.getElementById("y");
    minSpinner = document.getElementById("min");
    maxSpinner = document.getElementById("max");
    //Pour choisir la valeur par défaut du drop down select
    dropDown = document.getElementById("drop_down");
    for (let item in statusTypes) {
        if (isNaN(Number(item))) {
            let node = document.createElement("option");
            let textnode = document.createTextNode(item);
            node.appendChild(textnode);
            node.value = statusTypes[item];
            dropDown.appendChild(node); 
        }
    }

    dropDown.value = status;
    nameLabel.innerHTML = name;
    xSpinner.value = xSize;
    ySpinner.value = ySize;
    minSpinner.value = min;
    maxSpinner.value = max;
}


function calculateElementsDimensionsAndPositions(){
    //DIMENSIONS DU CANEVAS
    canvasWidth = $(window).height()*0.8;
    if(canvasWidth > $(window).width()*canMaxWidth){
        canvasWidth = $(window).width()*canMaxWidth;
    }

    //On ne peut pas modifier les dimensions avec CSS (canvas.style.width), ça ne fonctionne pas bien
    //J'ajoute des pixels supplémentaires pour être sur que tous les bords de tuiles seront dessinées
    canvas.setAttribute("width", canvasWidth + mapExtraBorder);
    canvas.setAttribute("height", canvasWidth + mapExtraBorder);
    
    //DIMENSIONS DES TUILES
    tileWidth = canvasWidth/12;

    //DIMENSIONS DU PANEL
    //5% = distance du canevas par rapport à la gauche
    let panelPosition = $(window).width()*0.10 + canvasWidth;
    panel.style.left = panelPosition + "px";
    //10% = distance du canevas par rapport à la gauche + distance du panel par rapport à la droite
    let panelWidth = $(window).width() - panelPosition - $(window).width()*0.10;
    panel.style.width = panelWidth + "px";
}

//Cette fonction va permettre de créer la map en lui passant la liste de toutes les tuiles et leurs paramèters
//Pour l'instant, la carte est remplie avec des valeurs par défaut. 
function createMap(){
    //On détruit toutes les tuiles
	for (let i = 0; i < tileList.length; i++) {
        let element = tileList[i];
        element.die();
    }
    if(player1 && player2){
        player1.tile = null;
        player2.tile = null;
    }
    tileList = [];
    for (let i = 0; i < xSize; i++) {
        for (let j = 0; j < ySize; j++) {
            tileList.push(new Tile(i, j));
        }
    }
}


//Cette fonction permet de dessiner tous les éléments graphiques
function generalTick(){
    ctx.clearRect(0, 0, canvasWidth + mapExtraBorder, canvasWidth + mapExtraBorder);

    //Permet de disable les keys quand on utilise le clavier
    if(document.getElementById("txt_name")){
        removeKeysListener();
    } else {
        initializeKeysListener();
    }
	for (let i = 0; i < tileList.length; i++) {
        const element = tileList[i];
        const alive = element.tick();
        if(!alive){
			tileList.splice(i, 1);
			i--;
        }
    }
    //On dessine le curseur après tout le reste pour qu'il soit affiché par dessus. 
    cursor.tick();

	window.requestAnimationFrame(generalTick);
}

function feedBack(message, success=false){
    let node = document.createElement("div");
    var textnode = document.createTextNode(message);
    node.appendChild(textnode);
    if(!success){
        node.style.color = "rgb(255, 0, 0)";
    } else {
        node.style.color = "rgb(0, 100, 0)";
    }
    chat.appendChild(node); 
}


//Permet de changer entre les différentes onglets de l'éditeur (Parametres, Balance)
function openTab(tabName) {
    let k = document.getElementsByClassName("tab");
    for (let i = 0; i < k.length; i++) {
        k[i].style.display = "none"; 
    }
    document.getElementById(tabName).style.display = "block"; 
}

function getTile(x, y){
    for (let i = 0; i < tileList.length; i++) {
        const tile = tileList[i];
        if(tile.x == x && tile.y == y){
            return tile;
        }
    }
    return null;
}


//Les tuiles se mettent à jour elles-mêmes, elles «meurent» si leurs coordonnées sont hors des dimensions saisies, mais
//on doit les ajouter soi-même s'il faut en ajouter. 

//Ajouts des tuiles lorsque l'on modifie les dimensions de la carte en x.
function changeX(){
    let x = xSpinner.value
    let difference = x - xSize;
    xSize = x;
    
    //Ici, on veut ajouter de nouvelles tuiles
    if(difference > 0){
        for (let i = 0; i < ySize; i++) {
            tileList.push(new Tile(xSize - 1, i));
        }
    }
}

//Ajouts des tuiles lorsque l'on modifie les dimensions de la carte en y. 
function changeY(){
    let y = ySpinner.value
    let difference = y - ySize;
    ySize = y;

    //Ici, on veut ajouter de nouvelles tuiles
    if(difference > 0){
        for (let i = 0; i < xSize; i++) {
            tileList.push(new Tile(i, ySize - 1));
        }
    }
}

function changeMin(){
    let tempMin = minSpinner.value;
    if(tempMin){
        if(tempMin < 0){
            minSpinner.value = 0;
            tempMin = 0;
        }
        if(tempMin > max){
            minSpinner.value = max;
            tempMin = max;
        }
        min = Math.round(tempMin * 100) / 100;
        minSpinner.value = min;
    } else {
        minSpinner.value = min;
    }
}

function changeMax(){
    let tempMax = maxSpinner.value;
    if(tempMax){
        if(tempMax < min){
            maxSpinner.value = min;
            tempMax = min;
        }
        if(tempMax > minMaxSpinnersMaximum){
            maxSpinner.value = minMaxSpinnersMaximum
            tempMax = minMaxSpinnersMaximum;      
        }
        max = Math.round(tempMax * 100) / 100;
        maxSpinner.value = max;
    } else {    
        maxSpinner.value = max;
    }
}

function setPlayer1(){
    cursor.setPlayer(player1);
}

function setPlayer2(){
    cursor.setPlayer(player2);
}

function setWallType(type){
    cursor.setWallType(type);
}

function setTree(){
    cursor.setTree();
}

function setStatus(type){
    status = dropDown.options[dropDown.selectedIndex].value;
}

function displayBalance(){
    $.ajax({
        type : "POST",
        url : "ajaxIndex.php",
        data : {
            balance_id: true
        }
    })
    .done(response => {      
        response = JSON.parse(response);
        drop_down_balance.innerHTML = null;
        response.forEach(element => {
            let node = document.createElement("option");
            let textnode = document.createTextNode(element);
            node.appendChild(textnode);
            node.value = element;
            drop_down_balance.appendChild(node); 
        });
        getBalance(balanceId);
        drop_down_balance.value = balanceId;
    });
}

function displayBalanceElement(nom, valeur){
    let node = document.createElement("div");

    let name = document.createTextNode(nom + ": ");
    let span = document.createElement('span');
    span.style.fontWeight = "bold";
    span.appendChild(name);
    node.appendChild(span);

    let val = document.createTextNode(valeur);
    let span2 = document.createElement('span');
    span2.appendChild(val);
    node.appendChild(span2);

    balance_list.appendChild(node);
}

function getBalance(id){
    balance_list.innerHTML = null;
    if(id == null){
        balanceId = drop_down_balance.value;
    } else {
        balanceId = id;
    }
    $.ajax({
        type : "POST",
        url : "ajaxIndex.php",
        data : {
            balance: parseInt(balanceId)
        }
    })
    .done(response => {
        response = JSON.parse(response);
        response.forEach(element => {
            element.forEach(node => {
                displayBalanceElement(node.NOM, node.VALEUR);
            });
        });
    });
}

function stringifiableTileList(){
    let list = []
    for (let i = 0; i < tileList.length; i++) {
        if(!tileList[i].isEmpty()){
            let joueur = 0;
            if(tileList[i].player){
                joueur = tileList[i].player.number;
            }
            let jsonTile = {};
            jsonTile['nom'] = tileList[i].type;
            jsonTile['joueur'] = joueur;
            jsonTile['arbre'] = tileList[i].tree;
            jsonTile['x'] = tileList[i].x;
            jsonTile['y'] = tileList[i].y;
            list.push(jsonTile);
        }
    }
    return list;
}

function saveMap(){
    if(player1.isTileSetted() && player2.isTileSetted()){
        let list = stringifiableTileList();
        let data = {};
        data['tuiles'] = list;
        data['status'] = status;
        data['nom'] = name;
        data['delaiMin'] = min;
        data['delaiMax'] = max;
        data["nb_tuiles_x"] = parseInt(xSize);
        data["nb_tuiles_y"] = parseInt(ySize);
        data["balance"] = parseInt(balanceId);
        data = JSON.stringify(data);
        $.ajax({
            type : "POST",
            url : "ajaxIndex.php",
            data : {
                saveMap : true,
                infos: data
            }
        })
        .done(response => {
            response = JSON.parse(response);
            if(response == "OK"){
                feedBack("Votre carte a bien été sauvegardée.", true);
            } else if(response == "NOM_EXISTANT"){
                feedBack("Le nom de la carte existe déjà. La carte n'a pas été sauvegardée.");
            } else if(response == "ERREUR_INSERTION"){
                feedBack("Erreur d'insertion dans la base de données. Allez voir le DBA. ");
            } else {
                response.forEach(element => {
                    feedBack(element);
                });
            }
        });
    } else {
        feedBack("Vous devez placer les joueurs 1 et 2.");
    }
}