-- Tankem
-- Equipe: Mathieu Beland
--         Patrick Larose
--         Ibrahim Assadik El-Hedhiri

-- Il faut que la structure de fichiers respecte celle ci-dessous pour
-- que ce script fonctionne
PROMPT CREATION DES TABLE
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_level_editor/sql_balance/creationTableNiveau.sql"
COMMIT;

PROMPT CREATION DES PROCÉDURES
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_level_editor/sql_balance/tankem_procedures.sql"
COMMIT;

PROMPT CREATION DES TRIGGERS
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_level_editor/sql_balance/tankem_triggers.sql"
COMMIT;

PROMPT INSERTIONS DES DONNEES
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_level_editor/sql_balance/tankem_insertions.sql"
COMMIT;

PROMPT INSERTIONS DES NIVEAUX
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_level_editor/sql_balance/tankem_base_levels.sql"
COMMIT;