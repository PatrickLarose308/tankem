PROMPT \
PROMPT =========================================================================
PROMPT Destruction du packages
PROMPT =========================================================================
PROMPT /
DROP PACKAGE pkg_tankem_level_editor;

PROMPT \
PROMPT =========================================================================
PROMPT Cr�ation du packages avec les proc�dures
PROMPT =========================================================================
PROMPT /
CREATE OR REPLACE PACKAGE pkg_tankem_level_editor AS
    PROCEDURE proc_insertion_niveau(
        i_status        IN NomStatus.nom%TYPE,
        i_balance       IN ParametreNumerique.balance%TYPE,
        i_nom           IN Niveau.nom%TYPE,
        i_date_creation IN VARCHAR2,
        i_delai_min     IN Niveau.delai_min%TYPE,
        i_delai_max     IN Niveau.delai_max%TYPE,
        i_nb_tuiles_x   IN Niveau.nb_tuiles_x%TYPE,
        i_nb_tuiles_y   IN Niveau.nb_tuiles_y%TYPE
    );
    
    PROCEDURE proc_insertion_tuile(
        i_nom           IN NomTuile.nom%TYPE,
        i_niveau        IN Niveau.nom%TYPE,
        i_x             IN Tuile.x%TYPE,
        i_y             IN Tuile.y%TYPE,
        i_joueur        IN Tuile.joueur%TYPE,
        i_arbre         IN Tuile.arbre%TYPE
    );
END pkg_tankem_level_editor;
/

CREATE OR REPLACE PACKAGE BODY pkg_tankem_level_editor AS 
-- Procedure d'insertion d'une valeur numerique
     PROCEDURE proc_insertion_niveau(
        i_status        IN NomStatus.nom%TYPE,
        i_balance       IN ParametreNumerique.balance%TYPE,
        i_nom           IN Niveau.nom%TYPE,
        i_date_creation IN VARCHAR2,
        i_delai_min     IN Niveau.delai_min%TYPE,
        i_delai_max     IN Niveau.delai_max%TYPE,
        i_nb_tuiles_x   IN Niveau.nb_tuiles_x%TYPE,
        i_nb_tuiles_y   IN Niveau.nb_tuiles_y%TYPE
    )
    IS
        i_status_id Niveau.status_id%TYPE;
        i_date Niveau.date_creation%TYPE;
    BEGIN
        SELECT id
        INTO i_status_id
        FROM NomStatus
        WHERE NomStatus.nom = i_status;
        
        i_date := TO_DATE(i_date_creation, 'yyyy/mm/dd');
        
        INSERT INTO Niveau(status_id,balance_id,nom,date_creation,delai_min,delai_max,nb_tuiles_x,nb_tuiles_y)
        VALUES(i_status_id,i_balance,i_nom,i_date,i_delai_min,i_delai_max,i_nb_tuiles_x,i_nb_tuiles_y);
        
        COMMIT;
    END proc_insertion_niveau; 
  
    PROCEDURE proc_insertion_tuile(
        i_nom           IN NomTuile.nom%TYPE,
        i_niveau        IN Niveau.nom%TYPE,
        i_x             IN Tuile.x%TYPE,
        i_y             IN Tuile.y%TYPE,
        i_joueur        IN Tuile.joueur%TYPE,
        i_arbre         IN Tuile.arbre%TYPE
    )
    IS
        i_nom_id Tuile.nom_id%TYPE;
        i_niveau_id Tuile.niveau_id%TYPE;
    BEGIN
        SELECT id
        INTO i_nom_id
        FROM NomTuile
        WHERE NomTuile.nom = i_nom;
        
        SELECT id
        INTO i_niveau_id
        FROM Niveau
        WHERE Niveau.nom = i_niveau;
        
        INSERT INTO Tuile(nom_id, niveau_id, x, y, joueur, arbre)
        VALUES(i_nom_id, i_niveau_id, i_x, i_y, i_joueur, i_arbre);
        
        COMMIT;
    END proc_insertion_tuile;
END pkg_tankem_level_editor; 
/