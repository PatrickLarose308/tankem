-- Les tables de ce script sont normalise selon les trois premieres lois normales :
-- 1- Les donnees dans les tables sont atomiques, donc la premiere lois est respectee.
-- 2- Il n'y a qu'une seule cle primaire pour chaque table (le id), donc la deuxieme
-- 	  loi est respectee.
-- 3- Les champs non cle des tables dependent directement de la clee primaire, sans 
--	  intermediaire, donc la troisieme lois est respectee.      

PROMPT \
PROMPT =========================================================================
PROMPT Destruction des sequences
PROMPT =========================================================================
PROMPT /
DROP SEQUENCE seq_tuile;
DROP SEQUENCE seq_niveau;
DROP SEQUENCE seq_nom_tuile;
DROP SEQUENCE seq_nom_status;

PROMPT \
PROMPT =========================================================================
PROMPT Destruction des tables
PROMPT =========================================================================
PROMPT /
DROP TABLE Tuile;
DROP TABLE Niveau;
DROP TABLE NomTuile;
DROP TABLE NomStatus;

PROMPT \
PROMPT =========================================================================
PROMPT Creation des sequences
PROMPT =========================================================================
PROMPT /
CREATE SEQUENCE seq_niveau
NOCYCLE NOCACHE;
CREATE SEQUENCE seq_tuile
NOCYCLE NOCACHE;
CREATE SEQUENCE seq_nom_tuile
NOCYCLE NOCACHE;
CREATE SEQUENCE seq_nom_status
NOCYCLE NOCACHE;

PROMPT \
PROMPT =========================================================================
PROMPT Creation des tables
PROMPT =========================================================================
PROMPT /
CREATE TABLE NomStatus (
    id              NUMBER(10,0)    DEFAULT(seq_nom_status.nextval),
    nom             VARCHAR2(20)    NOT NULL,
    
    CONSTRAINT pk_nom_status PRIMARY KEY(id),
    CONSTRAINT cc_nom_status_nom CHECK(nom IN ('actif','test','inactif'))
);

CREATE TABLE NomTuile (
    id              NUMBER(10,0)    DEFAULT(seq_nom_tuile.nextval),
    nom             VARCHAR2(20)    NOT NULL,
    
    CONSTRAINT pk_nom_tuile PRIMARY KEY(id),
    CONSTRAINT cc_nom_tuile_nom CHECK(nom IN ('plancher','mur_fixe','mur_vertical','mur_inverse'))
);

CREATE TABLE Tuile (
    id              NUMBER(10,0)    DEFAULT(seq_tuile.nextval),
    nom_id          NUMBER(10,0)    NOT NULL,
    niveau_id       NUMBER(10,0)    NOT NULL,
    x               NUMBER(2,0)     NOT NULL,
    y               NUMBER(2,0)     NOT NULL,
    joueur          NUMBER(10,0)    NOT NULL,
    arbre           NUMBER(1,0)     NOT NULL,
    
    CONSTRAINT pk_tuile PRIMARY KEY(id),
    CONSTRAINT cc_tuile_x CHECK(x <= 12),
    CONSTRAINT cc_tuile_y CHECK(y <= 12),
    CONSTRAINT uc_tuile_xyniv UNIQUE(niveau_id,x,y),
    CONSTRAINT cc_tuile_arbre CHECK(arbre IN (0,1))
);

CREATE TABLE Niveau (
    id              NUMBER(10,0)    DEFAULT(seq_niveau.nextval),
    status_id       NUMBER(10,0)    NOT NULL,
    balance_id      NUMBER(10,0)    NOT NULL,
    nom             VARCHAR2(20)    NOT NULL,
    date_creation   DATE            NOT NULL,
    delai_min       NUMBER(8,2)     NOT NULL,
    delai_max       NUMBER(8,2)     NOT NULL,
    nb_tuiles_x     NUMBER(10,0)     NOT NULL,
    nb_tuiles_y     NUMBER(10,0)     NOT NULL,
        
    CONSTRAINT pk_niveau PRIMARY KEY(id),
	CONSTRAINT uc_niveau_nom UNIQUE(nom),
    CONSTRAINT cc_niveau_delai_min CHECK(delai_min > 0),
    CONSTRAINT cc_niveau_delai_max CHECK(delai_max > 0),
    CONSTRAINT cc_niveau_nb_tuiles_x CHECK(nb_tuiles_x >= 6 AND nb_tuiles_x <= 12),
    CONSTRAINT cc_niveau_nb_tuiles_y CHECK(nb_tuiles_y >= 6 AND nb_tuiles_y <= 12)
);

PROMPT =========================================================================
PROMPT Ajout des cles etrangeres
PROMPT =========================================================================
ALTER TABLE Tuile ADD CONSTRAINT fk_tuile_nom FOREIGN KEY(nom_id) REFERENCES NomTuile(id);
ALTER TABLE Tuile ADD CONSTRAINT fk_tuile_niveau FOREIGN KEY(niveau_id) REFERENCES Niveau(id);
ALTER TABLE Niveau ADD CONSTRAINT fk_niveau_status FOREIGN KEY(status_id) REFERENCES NomStatus(id);
ALTER TABLE Niveau ADD CONSTRAINT fk_niveau_balance FOREIGN KEY(balance_id) REFERENCES Balance(id);