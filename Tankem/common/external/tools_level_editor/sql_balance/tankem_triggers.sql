SET SERVEROUTPUT ON FORMAT WRAPPED;
PROMPT \
PROMPT =========================================================================
PROMPT Destruction des triggers
PROMPT =========================================================================
PROMPT /
DROP TRIGGER trg_before_niv_insr;
DROP TRIGGER trg_before_tui_insr;

PROMPT \
PROMPT =========================================================================
PROMPT Cr�ation des triggers
PROMPT =========================================================================
PROMPT /

CREATE OR REPLACE TRIGGER trg_before_niv_insr
BEFORE INSERT OR UPDATE
ON Niveau
FOR EACH ROW

DECLARE
    t_resultat_balance NUMBER;
    t_error VARCHAR2(255);
    
BEGIN
    t_error := 'ok';
    
    -- V�rification des delais d'apparition des objets
    IF (:new.delai_min > :new.delai_max) THEN
        t_error := 'Le d�lai minimum doit �tre sup�rieur au d�lai maximum';
    END IF;
    
    -- V�rification de l'existance de la balance
    BEGIN
        SELECT balance
        INTO t_resultat_balance
        FROM ParametreCaractere
        WHERE balance = :new.balance_id
        FETCH FIRST 1 ROW ONLY;
        
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            t_error := 'La balance doit exister';
    END;
    
    -- V�rifier la date
    IF (:new.date_creation > SYSDATE) THEN
        t_error := 'La date de cr�ation ne peut pas �tre dans le futur';
    END IF;
    
    -- Si l'insertion n'est pas valide
    IF (t_error != 'ok') THEN
        RAISE_APPLICATION_ERROR(-20001, t_error);
    END IF;
END;
/

CREATE OR REPLACE TRIGGER trg_before_tui_insr
BEFORE INSERT OR UPDATE
ON Tuile
FOR EACH ROW
BEGIN
    IF (:new.joueur != 0 AND :new.arbre = 1) THEN
        RAISE_APPLICATION_ERROR(-20001, 'Un joueur ne peut pas commencer sur une case avec un arbre');
    END IF;
END;
/