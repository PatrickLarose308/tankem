PROMPT \
PROMPT =========================================================================
PROMPT Insertions tests
PROMPT =========================================================================
PROMPT /

-- Effacer les parametres existant pour la partie 1
DELETE FROM Tuile;
DELETE FROM Niveau;
DELETE FROM NomStatus;
DELETE FROM NomTuile;

-- Insertion des noms de status et de tuile
INSERT INTO NomStatus(nom) VALUES ('actif');
INSERT INTO NomStatus(nom) VALUES ('test');
INSERT INTO NomStatus(nom) VALUES ('inactif');
INSERT INTO NomTuile(nom) VALUES ('plancher');
INSERT INTO NomTuile(nom) VALUES ('mur_fixe');
INSERT INTO NomTuile(nom) VALUES ('mur_vertical');
INSERT INTO NomTuile(nom) VALUES ('mur_inverse');