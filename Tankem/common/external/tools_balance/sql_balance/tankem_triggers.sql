SET SERVEROUTPUT ON FORMAT WRAPPED;
PROMPT \
PROMPT =========================================================================
PROMPT Destruction des table temporaire
PROMPT =========================================================================
PROMPT /
DROP TABLE V_ParametreNumerique;
DROP TABLE V_ParametreCaractere;

PROMPT \
PROMPT =========================================================================
PROMPT Cr�ation des table temporaire
PROMPT =========================================================================
PROMPT /
CREATE TABLE V_ParametreNumerique AS SELECT * FROM ParametreNumerique WHERE 0=1;
CREATE TABLE V_ParametreCaractere AS SELECT * FROM ParametreCaractere WHERE 0=1;

PROMPT \
PROMPT =========================================================================
PROMPT Destruction des triggers
PROMPT =========================================================================
PROMPT /
DROP TRIGGER trg_before_num_insr;
DROP TRIGGER trg_before_char_insr;

PROMPT \
PROMPT =========================================================================
PROMPT Cr�ation des triggers
PROMPT =========================================================================
PROMPT /

CREATE OR REPLACE TRIGGER trg_num_temp
BEFORE INSERT OR UPDATE
ON ParametreNumerique
FOR EACH ROW
BEGIN
    INSERT INTO V_ParametreNumerique(id, nom_id,valeur,categorie,balance)
    VALUES(:new.id, :new.nom_id,:new.valeur,:new.categorie,:new.balance);
END;
/

CREATE OR REPLACE TRIGGER trg_before_num_insr
AFTER INSERT OR UPDATE
ON ParametreNumerique

DECLARE
    t_valeur_min NUMBER;
    t_valeur_max NUMBER;
    t_valeur NUMBER;
    t_error VARCHAR2(255);
    t_double NUMBER;
    new_id NUMBER;
    new_nom_id NUMBER;
    new_valeur NUMBER;
    new_categorie NUMBER;
    new_balance NUMBER;

BEGIN
    -- Aller chercher les nouvelles valeurs
    SELECT id INTO new_id FROM V_ParametreNumerique;
    SELECT nom_id INTO new_nom_id FROM V_ParametreNumerique;
    SELECT valeur INTO new_valeur FROM V_ParametreNumerique;
    SELECT categorie INTO new_categorie FROM V_ParametreNumerique;
    SELECT balance INTO new_balance FROM V_ParametreNumerique;    
    
    -- Vider la table temporaire
    DELETE FROM V_ParametreNumerique;
    
    -- Valeur d'un min num�rique
    BEGIN
        SELECT valeur 
        INTO t_valeur_min
        FROM ParametreNumerique
        WHERE categorie = -1
        AND balance = new_balance
        AND nom_id = new_nom_id
        AND id != new_id;
    
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT -1 INTO t_valeur_min FROM DUAL;
        WHEN OTHERS THEN
            DBMS_OUTPUT.put_line(new_id);
    END;

    -- Valeur d'un max num�rique
    BEGIN
        SELECT valeur 
        INTO t_valeur_max
        FROM ParametreNumerique
        WHERE categorie = 1
        AND balance = new_balance
        AND nom_id = new_nom_id
        AND id != new_id;
    
    EXCEPTION 
        WHEN NO_DATA_FOUND THEN
            SELECT -1 INTO t_valeur_max FROM DUAL;
    END;
    
    -- Valeur d'une valeur num�rique
    BEGIN
        SELECT valeur 
        INTO t_valeur
        FROM ParametreNumerique
        WHERE categorie = 0
        AND balance = new_balance
        AND nom_id = new_nom_id
        AND id != new_id;
        
    EXCEPTION 
        WHEN NO_DATA_FOUND THEN
            SELECT -1 INTO t_valeur FROM DUAL;
    END;
    
    -- V�rification selon la categorie (min, max ou val)
    t_error := 'ok';
    t_double := 0;
    IF (t_valeur_min != -1) THEN
        IF (new_categorie != -1) THEN    
            IF (new_valeur < t_valeur_min) THEN
                t_error := 'La valeur doit �tre sup�rieure au minimum';
            END IF;
        ELSE
            t_double := 1;
        END IF;
    END IF;
    IF (t_valeur_max != -1) THEN
        IF (new_categorie != 1) THEN
            IF (new_valeur > t_valeur_max) THEN
                t_error := 'La valeur doit �tre inf�rieur au maximum';
            END IF;
        ELSE
            t_double := 1;
        END IF;
    END IF;
    IF (t_valeur != -1) THEN
        IF (new_categorie != 0) THEN
            IF (new_valeur > t_valeur AND new_categorie = -1) THEN
                t_error := 'La minimum doit �tre inf�rieur � la valeur actuelle';
            END IF;
            IF (new_valeur < t_valeur AND new_categorie = 1) THEN
                t_error := 'La maximum doit �tre sup�rieur � la valeur actuelle';
            END IF;
        ELSE
            t_double := 1;
        END IF;
    END IF;
    
    -- Si succ�s de l'insertion
    IF (t_error = 'ok') THEN    
        -- Suppression du doublon
        IF (t_double = 1) THEN
            DELETE FROM ParametreNumerique
            WHERE balance = new_balance
            AND categorie = new_categorie
            AND nom_id = new_nom_id
            AND id != new_id;
        END IF;
    -- Si �chec de l'insertion
    ELSE
        RAISE_APPLICATION_ERROR(-20001,t_error);
    END IF;
END;
/

CREATE OR REPLACE TRIGGER trg_char_temp
BEFORE INSERT OR UPDATE
ON ParametreCaractere
FOR EACH ROW
BEGIN
    INSERT INTO V_ParametreCaractere(id, nom_id,valeur,balance)
    VALUES(:new.id, :new.nom_id,:new.valeur,:new.balance);
END;
/

CREATE OR REPLACE TRIGGER trg_before_char_insr
AFTER INSERT OR UPDATE
ON ParametreCaractere

DECLARE
    t_double VARCHAR2(70);
    nom_id_temp NUMBER(10,0);
    new_id NUMBER(10,0);
    new_nom_id NUMBER(10,0);
    new_valeur VARCHAR2(70);
    new_balance NUMBER(10,0);
BEGIN

    -- Aller chercher les nouvelles valeurs
    SELECT id INTO new_id FROM V_ParametreCaractere;
    SELECT nom_id INTO new_nom_id FROM V_ParametreCaractere;
    SELECT valeur INTO new_valeur FROM V_ParametreCaractere;
    SELECT balance INTO new_balance FROM V_ParametreCaractere;    
    
    -- Vider la table temporaire
    DELETE FROM V_ParametreCaractere;

    -- V�rification si texte existant
    BEGIN
        SELECT valeur 
        INTO t_double
        FROM ParametreCaractere
        WHERE balance = new_balance
        AND nom_id = new_nom_id
        AND id != new_id;  
    EXCEPTION 
        WHEN NO_DATA_FOUND THEN
            t_double := NULL;
    END;
    
    -- V�rification de la longueur
    IF (LENGTH(new_valeur) > 70) THEN
        RAISE_APPLICATION_ERROR(-20001,'Le message doit avoir 70 caract�res ou moins');
    END IF;
    
    BEGIN
        SELECT id 
        INTO nom_id_temp 
        FROM NomParametre 
        WHERE nom = 'messageAccueil';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE('no data');
            nom_id_temp := '';
    END;
    IF (LENGTH(new_valeur) > 60 AND new_nom_id = nom_id_temp) THEN
        RAISE_APPLICATION_ERROR(-20001,'Le message d''accueil doit avoir 60 caract�res ou moins');
    END IF;
    
    BEGIN
        SELECT id 
        INTO nom_id_temp 
        FROM NomParametre 
        WHERE nom = 'messageSignalDebutPartie';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE('no data');
            nom_id_temp := '';
    END;
    IF (LENGTH(new_valeur) > 50 AND new_nom_id = nom_id_temp) THEN
        RAISE_APPLICATION_ERROR(-20001,'Le message de signal de d�but de partie doit avoir 50 caract�res ou moins');
    END IF;

    -- Suppression du doublon
    IF (t_double IS NOT NULL) THEN
        DELETE FROM ParametreCaractere
        WHERE balance = new_balance
        AND nom_id = new_nom_id
        AND id != new_id;
    END IF;
END;
/
