PROMPT \
PROMPT =========================================================================
PROMPT Insertions tests
PROMPT =========================================================================
PROMPT /

-- Effacer les parametres existant pour la balance 1
DELETE FROM ParametreNumerique WHERE balance = 1;
DELETE FROM ParametreCaractere WHERE balance = 1;
DELETE FROM NomParametre;

-- Insertion des noms de parametres
INSERT INTO NomParametre(nom,categorie) VALUES('vitesseChar',0);
INSERT INTO NomParametre(nom,categorie) VALUES('vitesseRotation',0);
INSERT INTO NomParametre(nom,categorie) VALUES('pointVieChar',0);
INSERT INTO NomParametre(nom,categorie) VALUES('tempsMouvementBloc',0);
INSERT INTO NomParametre(nom,categorie) VALUES('canonVitesseBalle',0);
INSERT INTO NomParametre(nom,categorie) VALUES('canonTempsRecharge',0);
INSERT INTO NomParametre(nom,categorie) VALUES('mitrailletteVitesseBalle',0);
INSERT INTO NomParametre(nom,categorie) VALUES('mitrailletteTempsRecharge',0);
INSERT INTO NomParametre(nom,categorie) VALUES('grenadeVitesseBalle',0);
INSERT INTO NomParametre(nom,categorie) VALUES('grenadeTempsRecharge',0);
INSERT INTO NomParametre(nom,categorie) VALUES('shotgunVitesseBalle',0);
INSERT INTO NomParametre(nom,categorie) VALUES('shotgunTempsRecharge',0);
INSERT INTO NomParametre(nom,categorie) VALUES('shotgunOuvertureFusil',0);
INSERT INTO NomParametre(nom,categorie) VALUES('piegeVitesseBalle',0);
INSERT INTO NomParametre(nom,categorie) VALUES('piegeTempsRecharge',0);
INSERT INTO NomParametre(nom,categorie) VALUES('missileVitesseBalle',0);
INSERT INTO NomParametre(nom,categorie) VALUES('missileTempsRecharge',0);
INSERT INTO NomParametre(nom,categorie) VALUES('springVitesseSaut',0);
INSERT INTO NomParametre(nom,categorie) VALUES('springTempsRecharge',0);
INSERT INTO NomParametre(nom,categorie) VALUES('grosseurExplosionBalle',0);
INSERT INTO NomParametre(nom,categorie) VALUES('messageAccueilDuree',0);
INSERT INTO NomParametre(nom,categorie) VALUES('messageCompteARebourDuree',0);
INSERT INTO NomParametre(nom,categorie) VALUES('messageAccueil',1);
INSERT INTO NomParametre(nom,categorie) VALUES('messageSignalDebutPartie',1);
INSERT INTO NomParametre(nom,categorie) VALUES('messageFinPartie',1);

BEGIN
    -- Insertion des valeurs numeriques
    pkg_tankem.proc_insertion_num('vitesseChar','10,0',-1,1);
    pkg_tankem.proc_insertion_num('vitesseChar','15,0',0,1);
    pkg_tankem.proc_insertion_num('vitesseChar','40,0',1,1);
    pkg_tankem.proc_insertion_num('vitesseRotation','1000,0',-1,1);
    pkg_tankem.proc_insertion_num('vitesseRotation','1500,0',0,1);
    pkg_tankem.proc_insertion_num('vitesseRotation','2000,0',1,1);
    pkg_tankem.proc_insertion_num('pointVieChar','100,0',-1,1);
    pkg_tankem.proc_insertion_num('pointVieChar','200,0',0,1);
    pkg_tankem.proc_insertion_num('pointVieChar','2000,0',1,1);
    pkg_tankem.proc_insertion_num('tempsMouvementBloc','0,2',-1,1);
    pkg_tankem.proc_insertion_num('tempsMouvementBloc','0,8',0,1);
    pkg_tankem.proc_insertion_num('tempsMouvementBloc','2,0',1,1);
    pkg_tankem.proc_insertion_num('canonVitesseBalle','4,0',-1,1);
    pkg_tankem.proc_insertion_num('canonVitesseBalle','14,0',0,1);
    pkg_tankem.proc_insertion_num('canonVitesseBalle','30,0',1,1);
    pkg_tankem.proc_insertion_num('canonTempsRecharge','0,2',-1,1);
    pkg_tankem.proc_insertion_num('canonTempsRecharge','1,2',0,1);
    pkg_tankem.proc_insertion_num('canonTempsRecharge','10,0',1,1);
    pkg_tankem.proc_insertion_num('mitrailletteVitesseBalle','4,0',-1,1);
    pkg_tankem.proc_insertion_num('mitrailletteVitesseBalle','18,0',0,1);
    pkg_tankem.proc_insertion_num('mitrailletteVitesseBalle','30,0',1,1);
    pkg_tankem.proc_insertion_num('mitrailletteTempsRecharge','0,2',-1,1);
    pkg_tankem.proc_insertion_num('mitrailletteTempsRecharge','0,4',0,1);
    pkg_tankem.proc_insertion_num('mitrailletteTempsRecharge','10,0',1,1);
    pkg_tankem.proc_insertion_num('grenadeVitesseBalle','10,0',-1,1);
    pkg_tankem.proc_insertion_num('grenadeVitesseBalle','16,0',0,1);
    pkg_tankem.proc_insertion_num('grenadeVitesseBalle','25,0',1,1);
    pkg_tankem.proc_insertion_num('grenadeTempsRecharge','0,2',-1,1);
    pkg_tankem.proc_insertion_num('grenadeTempsRecharge','0,8',0,1);
    pkg_tankem.proc_insertion_num('grenadeTempsRecharge','10,0',1,1);
    pkg_tankem.proc_insertion_num('shotgunVitesseBalle','4,0',-1,1);
    pkg_tankem.proc_insertion_num('shotgunVitesseBalle','13,0',0,1);
    pkg_tankem.proc_insertion_num('shotgunVitesseBalle','30,0',1,1);
    pkg_tankem.proc_insertion_num('shotgunTempsRecharge','0,2',-1,1);
    pkg_tankem.proc_insertion_num('shotgunTempsRecharge','0,8',0,1);
    pkg_tankem.proc_insertion_num('shotgunTempsRecharge','10,0',1,1);
    pkg_tankem.proc_insertion_num('shotgunOuvertureFusil','0,1',-1,1);
    pkg_tankem.proc_insertion_num('shotgunOuvertureFusil','0,4',0,1);
    pkg_tankem.proc_insertion_num('shotgunOuvertureFusil','1,5',1,1);
    pkg_tankem.proc_insertion_num('piegeVitesseBalle','0,2',-1,1);
    pkg_tankem.proc_insertion_num('piegeVitesseBalle','1,0',0,1);
    pkg_tankem.proc_insertion_num('piegeVitesseBalle','4,0',1,1);
    pkg_tankem.proc_insertion_num('piegeTempsRecharge','0,2',-1,1);
    pkg_tankem.proc_insertion_num('piegeTempsRecharge','0,8',0,1);
    pkg_tankem.proc_insertion_num('piegeTempsRecharge','10,0',1,1);
    pkg_tankem.proc_insertion_num('missileVitesseBalle','20,0',-1,1);
    pkg_tankem.proc_insertion_num('missileVitesseBalle','30,0',0,1);
    pkg_tankem.proc_insertion_num('missileVitesseBalle','40,0',1,1);
    pkg_tankem.proc_insertion_num('missileTempsRecharge','0,2',-1,1);
    pkg_tankem.proc_insertion_num('missileTempsRecharge','0,3',0,1);
    pkg_tankem.proc_insertion_num('missileTempsRecharge','10,0',1,1);
    pkg_tankem.proc_insertion_num('springVitesseSaut','6,0',-1,1);
    pkg_tankem.proc_insertion_num('springVitesseSaut','10,0',0,1);
    pkg_tankem.proc_insertion_num('springVitesseSaut','20,0',1,1);
    pkg_tankem.proc_insertion_num('springTempsRecharge','0,2',-1,1);
    pkg_tankem.proc_insertion_num('springTempsRecharge','0,5',0,1);
    pkg_tankem.proc_insertion_num('springTempsRecharge','10,0',1,1);
    pkg_tankem.proc_insertion_num('grosseurExplosionBalle','1,0',-1,1);
    pkg_tankem.proc_insertion_num('grosseurExplosionBalle','8,0',0,1);
    pkg_tankem.proc_insertion_num('grosseurExplosionBalle','30,0',1,1);
    pkg_tankem.proc_insertion_num('messageAccueilDuree','1,0',-1,1);
    pkg_tankem.proc_insertion_num('messageAccueilDuree','3,0',0,1);
    pkg_tankem.proc_insertion_num('messageAccueilDuree','10,0',1,1);
    pkg_tankem.proc_insertion_num('messageCompteARebourDuree','0,0',-1,1);
    pkg_tankem.proc_insertion_num('messageCompteARebourDuree','3,0',0,1);
    pkg_tankem.proc_insertion_num('messageCompteARebourDuree','10,0',1,1);
    
    -- Insertion des valeurs a caracteres
    pkg_tankem.proc_insertion_char('messageAccueil','Bienvenue les patates',1);
    pkg_tankem.proc_insertion_char('messageSignalDebutPartie','Go les patates !',1);
    pkg_tankem.proc_insertion_char('messageFinPartie','La patate gagnante est : ',1);
END;
/