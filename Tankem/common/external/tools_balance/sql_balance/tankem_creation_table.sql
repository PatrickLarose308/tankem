PROMPT \
PROMPT =========================================================================
PROMPT Destruction des sequences
PROMPT =========================================================================
PROMPT /
DROP SEQUENCE seq_parametreNumerique;
DROP SEQUENCE seq_parametreCaractere;
DROP SEQUENCE seq_nomParametre;

PROMPT \
PROMPT =========================================================================
PROMPT Destruction des indexs
PROMPT =========================================================================
PROMPT /
DROP INDEX idx_num_nom_id;
DROP INDEX idx_num_categorie;
DROP INDEX idx_num_balance;
DROP INDEX idx_num_balance_desc;
DROP INDEX idx_char_nom_id;
DROP INDEX idx_char_balance;

PROMPT \
PROMPT =========================================================================
PROMPT Destruction des tables
PROMPT =========================================================================
PROMPT /
DROP TABLE ParametreNumerique;
DROP TABLE ParametreCaractere;
DROP TABLE NomParametre;
DROP TABLE Balance;

PROMPT \
PROMPT =========================================================================
PROMPT Creation des sequences
PROMPT =========================================================================
PROMPT /
CREATE SEQUENCE seq_parametreNumerique
NOCYCLE NOCACHE;
CREATE SEQUENCE seq_parametreCaractere
NOCYCLE NOCACHE;
CREATE SEQUENCE seq_nomParametre
NOCYCLE NOCACHE;

PROMPT \
PROMPT =========================================================================
PROMPT Creation des tables
PROMPT =========================================================================
PROMPT /
CREATE TABLE Balance (
    id          NUMBER(10,0),
    
    CONSTRAINT pk_bal PRIMARY KEY(id)
);

CREATE TABLE ParametreNumerique (
    id          NUMBER(10,0)    DEFAULT(seq_parametreNumerique.nextval),
    nom_id      NUMBER(10,0)    NOT NULL,
    valeur      NUMBER(8,2)     NOT NULL,
    categorie   NUMBER(1,0)     NOT NULL,
    balance     NUMBER(10,0)    NOT NULL,
    
    CONSTRAINT pk_num PRIMARY KEY(id),
    CONSTRAINT cc_num_valeur CHECK(valeur >= 0),
    CONSTRAINT cc_num_categorie CHECK(categorie IN(-1,0,1))
);

CREATE TABLE ParametreCaractere (
    id          NUMBER(10,0)    DEFAULT(seq_parametreCaractere.nextval),
    nom_id      NUMBER(10,0)    NOT NULL,
    valeur      VARCHAR2(255)   NOT NULL,
    balance      NUMBER(10,0)    NOT NULL,
    
    CONSTRAINT pk_char PRIMARY KEY(id)
);

CREATE TABLE NomParametre (
    id          NUMBER(10,0)    DEFAULT(seq_nomParametre.nextval),
    nom         VARCHAR2(50)    NOT NULL,
    categorie   NUMBER(1,0)     NOT NULL,
    
    CONSTRAINT pk_nom PRIMARY KEY(id),
    CONSTRAINT uc_nom_nom UNIQUE(nom),
    CONSTRAINT cc_nom_cat CHECK(categorie IN(0,1))  
);

PROMPT =========================================================================
PROMPT Ajout des cles etrangeres
PROMPT =========================================================================
ALTER TABLE ParametreNumerique ADD CONSTRAINT fk_num_nom FOREIGN KEY(nom_id) REFERENCES NomParametre(id);
ALTER TABLE ParametreCaractere ADD CONSTRAINT fk_char_nom FOREIGN KEY(nom_id) REFERENCES NomParametre(id);
ALTER TABLE ParametreNumerique ADD CONSTRAINT fk_num_bal FOREIGN KEY(balance) REFERENCES Balance(id);
ALTER TABLE ParametreCaractere ADD CONSTRAINT fk_char_bal FOREIGN KEY(balance) REFERENCES Balance(id);

PROMPT \
PROMPT =========================================================================
PROMPT Creation des indexs
PROMPT =========================================================================
PROMPT /
CREATE INDEX idx_num_nom_id ON ParametreNumerique(nom_id);
CREATE INDEX idx_num_categorie ON ParametreNumerique(categorie);
CREATE INDEX idx_num_balance ON ParametreNumerique(balance);
CREATE INDEX idx_num_balance_desc ON ParametreNumerique('balance' desc);
CREATE INDEX idx_char_nom_id ON ParametreCaractere(nom_id);
CREATE INDEX idx_char_balance ON ParametreCaractere(balance);
