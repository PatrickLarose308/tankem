-- Tankem
-- Equipe: Mathieu Beland
--         Patrick Larose
--         Ibrahim Assadik El-Hedhiri

-- Il faut que la structure de fichiers respecte celle ci-dessous pour
-- que ce script fonctionne
PROMPT CREATION DES TABLE
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_balance/sql_balance/tankem_creation_table.sql"
COMMIT;

PROMPT CREATION DES PROCÉDURES
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_balance/sql_balance/tankem_procedures.sql"
COMMIT;

PROMPT CREATION DES TRIGGERS
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_balance/sql_balance/tankem_triggers.sql"
COMMIT;

PROMPT INSERTIONS DES DONNEES
@"C:/Travail/htdocs/tankem/Tankem/common/external/tools_balance/sql_balance/tankem_insertions.sql"
COMMIT;

