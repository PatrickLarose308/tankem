PROMPT \
PROMPT =========================================================================
PROMPT Destruction du packages
PROMPT =========================================================================
PROMPT /
DROP PACKAGE pkg_tankem;

PROMPT \
PROMPT =========================================================================
PROMPT Cr�ation du packages avec les proc�dures
PROMPT =========================================================================
PROMPT /
CREATE OR REPLACE PACKAGE pkg_tankem AS
    PROCEDURE proc_insertion_num(
        i_nom       IN NomParametre.nom%TYPE,
        i_valeur    IN ParametreNumerique.valeur%TYPE,
        i_categorie IN ParametreNumerique.categorie%TYPE,
        i_balance    IN ParametreNumerique.balance%TYPE
    ); 
  
    PROCEDURE proc_insertion_char(
        i_nom       IN NomParametre.nom%TYPE,
        i_valeur    IN ParametreCaractere.valeur%TYPE,
        i_balance    IN ParametreCaractere.balance%TYPE
    ); 
  
    PROCEDURE proc_derniere_balance(
        i_rep OUT parametrenumerique.balance%TYPE   
    );
END pkg_tankem;
/

CREATE OR REPLACE PACKAGE BODY pkg_tankem AS 
-- Procedure d'insertion d'une valeur numerique
    PROCEDURE proc_insertion_num(
        i_nom       IN NomParametre.nom%TYPE,
        i_valeur    IN ParametreNumerique.valeur%TYPE,
        i_categorie IN ParametreNumerique.categorie%TYPE,
        i_balance    IN ParametreNumerique.balance%TYPE
    ) 
    IS
        i_nom_id ParametreNumerique.nom_id%TYPE;
        i_new_balance NUMBER;
    BEGIN
        SELECT id
        INTO i_nom_id
        FROM NomParametre
        WHERE NomParametre.nom = i_nom;
        
        -- Creation de la balance is inexistante
        BEGIN
            SELECT id
            INTO i_new_balance
            FROM Balance
            WHERE id = i_balance;
        
        EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                INSERT INTO Balance(id) VALUES(i_balance);
        END;
        
        INSERT INTO ParametreNumerique(nom_id,valeur,categorie,balance)
        VALUES(i_nom_id,i_valeur,i_categorie,i_balance);
        
        COMMIT;
    END proc_insertion_num; 
  
-- Procedure d'insertion d'une valeur a caracteres
    PROCEDURE proc_insertion_char(
        i_nom       IN NomParametre.nom%TYPE,
        i_valeur    IN ParametreCaractere.valeur%TYPE,
        i_balance    IN ParametreCaractere.balance%TYPE
    ) 
    IS
        i_nom_id ParametreCaractere.nom_id%TYPE;
    BEGIN
        SELECT id
        INTO i_nom_id
        FROM NomParametre
        WHERE NomParametre.nom = i_nom;
      
        INSERT INTO ParametreCaractere(nom_id,valeur,balance)
        VALUES(i_nom_id,i_valeur,i_balance);
        
        COMMIT;
    END proc_insertion_char; 
  
-- Procedure pour trouver la balance avec le plus grand numero
-- Sera utilise lors de l'ecriture et de la lecture de la BD
    PROCEDURE proc_derniere_balance(
        i_rep OUT parametrenumerique.balance%TYPE   
    ) 
    IS
        i_derniere_balance NUMBER;
        i_nb_parametre_char NUMBER;
        i_nb_parametre_num NUMBER;
        i_confirmation NUMBER;
    BEGIN
        i_confirmation := 0;
  
        WHILE (i_confirmation = 0) LOOP
            SELECT id
            INTO i_rep
            FROM Balance
            ORDER BY id DESC
            FETCH FIRST 1 ROW ONLY;
            
            SELECT COUNT(*)
            INTO i_nb_parametre_num
            FROM ParametreNumerique
            WHERE balance = i_rep;
            
            SELECT COUNT(*)
            INTO i_nb_parametre_char
            FROM ParametreCaractere
            WHERE balance = i_rep;
            
            IF (i_nb_parametre_num = 66 AND i_nb_parametre_char = 3) THEN
                i_confirmation := 1;
            ELSE
                DELETE FROM ParametreNumerique WHERE balance = i_derniere_balance;
                DELETE FROM ParametreCaractere WHERE balance = i_derniere_balance;
                DELETE FROM Balance WHERE id = i_derniere_balance;
            END IF;                   
        END LOOP;
    EXCEPTION 
        WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20001,'Pas de balance valide');
            ROLLBACK;
    END proc_derniere_balance; 
END pkg_tankem; 
/