# coding: utf-8
import os,sys,inspect
import traceback
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
import internal.dao.balanceDAOcsv as daoCsv
import internal.dao.balanceDAOoracle as daoOracle

# Création des DAO 
balanceDAOcsv = daoCsv.BalanceDAOcsv()
balanceDAOoracle = daoOracle.BalanceDAOoracle()

# Aller chercher la connexion à la base de données
if balanceDAOoracle.getConnection():

	# Aller chercher le chemin et la connection
	balanceDAOcsv.findFilePath()

	if balanceDAOcsv.path:
		# Aller chercher les DTO
		try:
			balanceDAOcsv.select()

			# Sanitation
			if balanceDAOcsv.sanitizer.sanitizeAll(balanceDAOcsv.dto, balanceDAOcsv.dtoMin, balanceDAOcsv.dtoMax):
				print("Des erreurs se sont produites lors de la lecture des donnees. Veuillez consulter la liste d'erreurs ci-dessous.")
				print(balanceDAOcsv.sanitizer.errors)
				raw_input("Appuyer sur ENTER pour continuer")
			else:

				# Connexion à la BD 
				if balanceDAOoracle.getConnection():

					# Remplir DTO de Oracle
					balanceDAOoracle.dto = balanceDAOcsv.dto
					balanceDAOoracle.dtoMin = balanceDAOcsv.dtoMin
					balanceDAOoracle.dtoMax = balanceDAOcsv.dtoMax

					# Appel insertion
					try:
						balanceDAOoracle.insert()
						print("Insertion reussie!")
						raw_input("Appuyer sur ENTER pour continuer")
					except:
						print("Echec de l'insertion")
						raw_input("Appuyer sur ENTER pour continuer")

					# Fermeture connection
					balanceDAOoracle.closeConnection()

		except:
			print("L'un des parametres entres ne se retrouve pas dans la liste des parametres")
			raw_input("Appuyer sur ENTER pour continuer")

	else:
		print("Aucun fichier n'a ete selectionne")
		raw_input("Appuyer sur ENTER pour continuer")
else:
	print("Connexion impossible")
	raw_input("Appuyer sur ENTER pour continuer")