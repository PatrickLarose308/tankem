# coding: utf-8
import os,sys,inspect
import traceback
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir = os.path.dirname(parentdir)
sys.path.insert(0,parentdir)
import internal.dao.balanceDAOcsv as daoCsv
import internal.dao.balanceDAOoracle as daoOracle

# Création des DAO 
balanceDAOcsv = daoCsv.BalanceDAOcsv()
balanceDAOoracle = daoOracle.BalanceDAOoracle()

# Aller chercher la connexion à la base de données
if balanceDAOoracle.getConnection():

	# Aller chercher le chemin et la connection
	balanceDAOcsv.findFilePath()
	if (not balanceDAOcsv.path):
		print("Aucun fichier n'a ete selectionne")
		raw_input("Appuyer sur ENTER pour continuer")
	
	else:
		# Aller chercher les DTO
		balanceDAOoracle.select()
		balanceDAOcsv.dto = balanceDAOoracle.dto
		balanceDAOcsv.dtoMin = balanceDAOoracle.dtoMin
		balanceDAOcsv.dtoMax = balanceDAOoracle.dtoMax

		# Fermeture connection
		balanceDAOoracle.closeConnection()

		try:
			# Remplir le fichier csv avec les données de la BD
			balanceDAOcsv.insert()
			print("Succes de la lecture des donnees")
			raw_input("Appuyer sur ENTER pour continuer")

			# Ouvrir excel
			balanceDAOcsv.openExcel()
		except:
			print("Erreur lors de la lecture des donnees")
			traceback.print_exc()
			raw_input("Appuyer sur ENTER pour continuer")
else:
	print("Connexion impossible")
	#traceback.print_exc()
	raw_input("Appuyer sur ENTER pour continuer")