# coding: utf-8
import numbers
import decimal

class BalanceDTOFilter():
    def __init__(self):
        pass

    def filterNumericalAttribute(self, value, min, max, nomParametre, defaultValue):
        upperBound = 100000000
        lowerBound = 2
        #Dans la base de données, on a une précision de 2 décimales. Cependant, il est impossible de savoir avec précision combien de décimales
        #un float contient.
        #https://docs.python.org/2/tutorial/floatingpoint.html
        #Representation error refers to the fact that some (most, actually) decimal fractions cannot be represented exactly as binary (base 2) 
        # fractions. This is the chief reason why Python (or Perl, C, C++, Java, Fortran, and many others) often won’t display the exact decimal
        #  number you expect:
        #>>> 0.1 + 0.2
        #0.30000000000000004
        #En conséquence, si une valeur est un float, on va automatiquement le caper à 2 décimales.
        if type(value) == float:
            value = round(value, lowerBound)
        if type(min) == float:
            min = round(min, lowerBound)
        if type(max) == float:
            max = round(max, lowerBound)
        if type(defaultValue) == float:
            defaultValue = round(defaultValue, lowerBound)

        resultat = {
            'success': True,
            'value': value, 
            'min': min,
            'max': max,
            'errors': []
        }

        if type(value) !=  int and type(value) !=  float and type(value) !=  long and type(value) !=  complex:
            resultat['success'] = False
            resultat['errors'].append(nomParametre + ": la valeur passee(" + str(value) + ") n'est pas un nombre.")
        #Si la valeur n'est pas un String, on ne fait auxune autre vérification
        if resultat['success']:
            #En premier lieu, on vérifie si les min et max sont consistants. Si non, on met les min et max nulls et la valeur par défaut.
            if type(min) !=  int and type(min) !=  float and type(min) !=  long and type(min) !=  complex:
                resultat['success'] = False
                resultat['errors'].append(nomParametre + ": le minimum(" + str(min) + ") n'est pas un nombre.")
            if type(max) !=  int and type(max) !=  float and type(max) !=  long and type(max) !=  complex:
                resultat['success'] = False
                resultat['errors'].append(nomParametre + ": le maximum(" + str(max) + ") n'est pas un nombre.")
            if min < 0:
                resultat['success'] = False
                resultat['errors'].append(nomParametre + ": le minium(" + str(min) + ") est negatif.")
            if max < 0:
                resultat['success'] = False
                resultat['errors'].append(nomParametre + ": le maximum(" + str(max) + ") est negatif.")
            if min > max:
                resultat['success'] = False
                resultat['errors'].append(nomParametre + ": le maximum("+ str(max) + ") est plus petit que le minimum("+ str(min) + ").")
            if min >= upperBound:
                resultat['success'] = False
                resultat['errors'].append(nomParametre + ": le minimum("+ str(min) + ") doit se situer entre 99999999.99 et 0.01.")
            if max >= upperBound:
                resultat['success'] = False
                resultat['errors'].append(nomParametre + ": le minimum("+ str(max) + ") doit se situer entre 99999999.99 et 0.01")

            #Si les minimums et maximums sont conistants, on fait les vérifications pour la valeur par défaut.
            if resultat['success']:
                if value >= upperBound:
                    resultat['success'] = False
                    resultat['errors'].append(nomParametre + ": la valeur passee("+ str(value) + ") doit se situer entre 99999999.99 et 0.01.")
                if value < 0:
                    resultat['success'] = False
                    resultat['errors'].append(nomParametre + ": la valeur passee(" + str(value) + ") est negatif.")
                if value > max:
                    resultat['success'] = False
                    resultat['errors'].append(nomParametre + ": la valeur passee("+ str(value) + ") depasse le maxium(" + str(max) + "). La valeur est clampe au maximum")
                    resultat['value'] = max #On clamp la valeur au maximum
                if value < min:
                    resultat['success'] = False
                    resultat['errors'].append(nomParametre + ": la valeur passee("+ str(value) + ") est inferieure au minimum("+ str(min) + "). La valeur est clampe au minimum.")
                    resultat['value'] = min #On clamp la valeur au minimum
            else:
                #On met la valeur par défaut si les maximums et minimums sont inconsistants
                resultat['value'] = defaultValue
                resultat['min'] = None
                resultat['max'] = None
        else:
            #On met la valeur par défaut si la valeur passée en paramètre n'est pas un chiffre
            resultat['value'] = defaultValue
            resultat['min'] = None
            resultat['max'] = None
        return resultat

    def filterString(self, message, nbCharMax, nomParametre, defaultValue):
        resultat = {
            'success': True,
            'message': message,
            'errors': []
        }
        if not isinstance(message, basestring):
            resultat['success'] = False
            resultat['errors'].append(nomParametre + ": le message n'est pas une chaine de caracteres.")
        if not isinstance(nbCharMax, int):
            resultat['success'] = False
            resultat['errors'].append(nomParametre + ": le nombre de caracteres maximal(" + str(nbCharMax) + ") n'est pas un entier.")
        if resultat['success'] and len(message) > nbCharMax:
            resultat['success'] = False
            resultat['errors'].append(nomParametre + ": le nombre de caracteres(" + str(len(message)) + ") depasse le maximum(" + str(nbCharMax) + ").")
        if not resultat ['success']:
            resultat ['message'] = defaultValue
        return resultat