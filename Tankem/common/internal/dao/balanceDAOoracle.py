# coding: utf-8
import cx_Oracle
import os,sys,inspect
import traceback
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import id_oracle_account
from balanceDAO import BalanceDAO
from sanitizerBalanceDTO import SanitizerBalanceDTO
from dataTypes import DataTypes

class BalanceDAOoracle(BalanceDAO):
	"""
	Classe d'un DAO pour la base de données oracle
	"""

	def __init__(self):
		BalanceDAO.__init__(self)
		self.oracleConnection =None
		self.oracleCursor = None


	def getConnection(self):
		try:	
			self.oracleConnection = cx_Oracle.connect(id_oracle_account.id, id_oracle_account.password, 'delta/decinfo.edu')
			self.oracleCursor = self.oracleConnection.cursor()
			return True
		except:
			return False


	def closeConnection(self):
		self.oracleCursor.close()
		self.oracleConnection.close()


	def insert(self):
		# Aller chercher le numéro de balance et faire +1
		balancePlusUn = self.oracleCursor.var(cx_Oracle.NUMBER)
		balance = self.oracleCursor.var(cx_Oracle.NUMBER)
		self.oracleCursor.callproc("pkg_tankem.proc_derniere_balance", [balance])
		temp = balance.getvalue() + 1
		balancePlusUn.setvalue(0,temp)

		# Variables oracle pour catégories
		catVal = self.oracleCursor.var(cx_Oracle.NUMBER)
		catVal.setvalue(0, 0)
		
		catMin = self.oracleCursor.var(cx_Oracle.NUMBER)
		catMin.setvalue(0, -1)
		
		catMax = self.oracleCursor.var(cx_Oracle.NUMBER)
		catMax.setvalue(0, 1)

		# Appel de la méthode insertDto avec bonnes valeurs
		self.insertDto(self.dto, catVal, balancePlusUn)
		self.insertDto(self.dtoMin, catMin, balancePlusUn)
		self.insertDto(self.dtoMax, catMax, balancePlusUn)
	
	def insertDto(self, dto, categorie, balancePlusUn):
		# Variables oracle à remplir
		nomVar = self.oracleCursor.var(cx_Oracle.STRING)
		valNum = self.oracleCursor.var(cx_Oracle.NUMBER)
		valStr = self.oracleCursor.var(cx_Oracle.STRING)

		for key in dto.__dict__:
			nomVar.setvalue(0, key)
			if self.sanitizer.defaultValues[key][1] is DataTypes.NUMERICAL:
				valNum.setvalue(0, dto.__dict__[key])
				self.oracleCursor.callproc("pkg_tankem.proc_insertion_num", [nomVar, valNum, categorie, balancePlusUn])
			elif self.sanitizer.defaultValues[key][1] is DataTypes.TEXT and categorie.getvalue() == 0 :
				valStr.setvalue(0, dto.__dict__[key])
				self.oracleCursor.callproc("pkg_tankem.proc_insertion_char", [nomVar, valStr, balancePlusUn])

	def select(self, balance_num=None):
		# Aller chercher le numéro de balance
		if not balance_num:
			balance = self.oracleCursor.var(cx_Oracle.NUMBER)
			self.oracleCursor.callproc("pkg_tankem.proc_derniere_balance", [balance])
		else:
			balance = self.oracleCursor.var(cx_Oracle.NUMBER)
			balance.setvalue(0,balance_num)

		# Remplir les DTO
		self.fillDto(0, self.dto, balance)
		self.fillDto(-1, self.dtoMin, balance)
		self.fillDto(1, self.dtoMax, balance)
		
		# Sanitation des données
		self.sanitizer.sanitizeAll(self.dto, self.dtoMin, self.dtoMax)

	def fillDto(self, categorie, dto, balance):
		selectNumerique = "SELECT valeur FROM ParametreNumerique WHERE balance = " + str(balance.getvalue()) + " AND categorie = :categorie AND nom_id = (SELECT id FROM NomParametre WHERE nom = :nom)"
		selectText = "SELECT valeur FROM ParametreCaractere WHERE balance = " + str(balance.getvalue()) + " AND nom_id = (SELECT id FROM NomParametre WHERE nom = :nom)"
		for key in self.sanitizer.defaultValues.keys():
			if self.sanitizer.defaultValues[key][1] is DataTypes.NUMERICAL:
				valeurs={':categorie':categorie, ':nom':key}
				self.oracleCursor.execute(selectNumerique, valeurs)
			elif self.sanitizer.defaultValues[key][1] is DataTypes.TEXT and categorie is 0 :
				valeurs={':nom':key}
				self.oracleCursor.execute(selectText, valeurs)
			query_result = self.oracleCursor.fetchall()
			if query_result:
				dto.__dict__[key] = query_result[0][0]