# coding: utf-8
from balanceDTO import BalanceDTO
from sanitizerBalanceDTO import SanitizerBalanceDTO

class BalanceDAO(object):
	"""
	Classe abstraite d'un DAO
	"""

	def __init__(self):
		self.sanitizer = SanitizerBalanceDTO()
		self.dto = self.sanitizer.initializeDTOBalance()
		self.dtoMin = self.sanitizer.initializeDTOBalance()
		self.dtoMax = self.sanitizer.initializeDTOBalance()

	def insert(self):
		raise NotImplementedError("Subclass must implement abstract method.")
	
	def select(self):
		raise NotImplementedError("Subclass must implement abstract method.")