# coding: utf-8
from balanceDTOFilter import BalanceDTOFilter
from balanceDTO import BalanceDTO
from dataTypes import DataTypes

class SanitizerBalanceDTO():
	def __init__(self):
		self.filtre = BalanceDTOFilter()
		self.errors = []
		self.defaultValues = {
            'vitesseChar': (7.0, DataTypes.NUMERICAL),
		    'vitesseRotation': (1500.0, DataTypes.NUMERICAL),
		    'pointVieChar': (200.0, DataTypes.NUMERICAL),
		    'tempsMouvementBloc': (0.8, DataTypes.NUMERICAL),
		    'canonVitesseBalle': (14.0, DataTypes.NUMERICAL),
		    'canonTempsRecharge': (1.2, DataTypes.NUMERICAL),
		    'mitrailletteVitesseBalle': (18.0, DataTypes.NUMERICAL),
		    'mitrailletteTempsRecharge': (0.4, DataTypes.NUMERICAL),
		    'grenadeVitesseBalle': (16.0, DataTypes.NUMERICAL),
		    'grenadeTempsRecharge': (0.8, DataTypes.NUMERICAL),
		    'shotgunVitesseBalle': (13.0, DataTypes.NUMERICAL),
		    'shotgunTempsRecharge': (1.8, DataTypes.NUMERICAL),
		    'shotgunOuvertureFusil': (0.4, DataTypes.NUMERICAL),
		    'piegeVitesseBalle': (1.0, DataTypes.NUMERICAL),
		    'piegeTempsRecharge': (0.8, DataTypes.NUMERICAL),
		    'missileVitesseBalle': (30.0, DataTypes.NUMERICAL),
		    'missileTempsRecharge': (3.0, DataTypes.NUMERICAL),
		    'springVitesseSaut': (10.0, DataTypes.NUMERICAL),
		    'springTempsRecharge': (0.5, DataTypes.NUMERICAL),
		    'grosseurExplosionBalle': (8.0, DataTypes.NUMERICAL),
		    'messageAccueil': ("Tankem!", DataTypes.TEXT, 60),
		    'messageAccueilDuree': (3.0, DataTypes.NUMERICAL),
		    'messageCompteARebourDuree': (3.0, DataTypes.NUMERICAL),
		    'messageSignalDebutPartie': ("Appuyer sur F1 pour l'aide!", DataTypes.TEXT, 50),
		    'messageFinPartie': ("Joueur %s a gagné!", DataTypes.TEXT, 70)
        }
	def isDTOIdentical(self, dto):
		identical = True
		differences = ["Aucune differences"]
		if not isinstance(dto, BalanceDTO):
			identical = False
			self.errors.append("L'objet passe n'est pas une instance de BalanceDTO")
			return identical

		#On vérifie si les clés du DTO sont dans le dictionnaire du Sanitizer.
		for entry in dto.__dict__:
			if not entry in self.defaultValues.keys():
				identical = False
				differences[0] = "Le dto contient n'est pas identique au sanitizer"
				differences.append(entry)
				
		#On vérifie si les clés du Sanitizer sont dans le dictionnaire du DTO.
		for entry in self.defaultValues.keys():
			if not entry in dto.__dict__:
				identical = False
				differences[0] = "Le dto contient n'est pas identique au sanitizer"
				differences.append(entry)
		if not identical:
			self.errors.append(differences)
		return identical

	def sanitizeAll(self, dtoValue, dtoMin, dtoMax):
		self.errors =[]

		#On vérifie si les attributs des 3 DTO sont identiques à ceux du sanitizer
		if not self.isDTOIdentical(dtoValue) or not self.isDTOIdentical(dtoMin) or not self.isDTOIdentical(dtoMax):
			raise ValueError(self.errors)
		for key in self.defaultValues.keys():
			if self.defaultValues[key][1] == DataTypes.NUMERICAL:
				resultatNum = self.filtre.filterNumericalAttribute(dtoValue.__dict__[key], dtoMin.__dict__[key], dtoMax.__dict__[key], key, self.defaultValues[key][0])
				dtoValue.__dict__[key] = resultatNum['value']
				dtoMin.__dict__[key] = resultatNum['min']
				dtoMax.__dict__[key] = resultatNum['max']
				if(not resultatNum['success']):
					self.errors.append(resultatNum['errors'])
			elif self.defaultValues[key][1] == DataTypes.TEXT:
				resultatString = self.filtre.filterString(dtoValue.__dict__[key], self.defaultValues[key][2], key, self.defaultValues[key][0])
				dtoValue.__dict__[key] = resultatString['message']
				if(not resultatString['success']):
					self.errors.append(resultatString['errors'])
			else: 
				raise ValueError("Le type de l'attribut ne fait pas partie de l'enumeration dataTypes")
		return self.errors

	def displayErrorsInConsole(self):
		for error in self.errors:
			print(error)

	def initializeDTOBalance(self):
		dto = BalanceDTO()
		for entry in self.defaultValues.keys():
			if not entry in dto.__dict__:
				setattr(dto, entry, None)
		return dto
	
	def initializeDTOBalanceWithDefaultValues(self):
		dto = self.initializeDTOBalance()
		for entry in self.defaultValues.keys():
			dto.__dict__[entry] = self.defaultValues[entry][0]
		return dto 