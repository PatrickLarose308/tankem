# coding: utf-8
import os
import csv
import Tkinter
import tkFileDialog
root = Tkinter.Tk()
root.withdraw()
from balanceDAO import BalanceDAO
from dataTypes import DataTypes

class BalanceDAOcsv(BalanceDAO):
	"""
	Classe d'un DAO pour les fichiers CSV
	"""

	def __init__(self):
		BalanceDAO.__init__(self)
		self.path = None
		

	def insert(self):
		# Préparer le ficher csv
		with open(self.path, 'w') as fic:
			writer = csv.writer(fic, delimiter=';',quotechar='|', lineterminator='\n', quoting=csv.QUOTE_MINIMAL)
			writer.writerow(["Attribut", "Valeur", "Min", "Max"])

			# Aller chercher les valeurs dans les dto dans le bon format et les mettre en ordre
			tab = []
			for key in self.dtoMin.__dict__:
				if self.dtoMin.__dict__[key] is None:
					tab.append([key, self.dto.__dict__[key], "-", "-"])
				else:
					tab.append([key, self.dto.__dict__[key], self.dtoMin.__dict__[key], self.dtoMax.__dict__[key]])
			tab = sorted(tab, key=self.getKey)

			# Écrire les attributs du DTO dans le fichier csv
			for line in tab:	
				writer.writerow(line)

	def getKey(self, item):
		return item[0]
	
	def select(self):
		# Préparer le ficher csv
		with open(self.path, 'r') as fic:
			reader = csv.reader(fic, delimiter=';',quotechar='|', lineterminator='\n', quoting=csv.QUOTE_MINIMAL)
			
			#sauter première ligne
			reader.next()
			
			for row in reader:
				if self.sanitizer.defaultValues[row[0]][1] is DataTypes.NUMERICAL:
					try:
						self.dto.__dict__[row[0]] = float(row[1])
					except:
						self.dto.__dict__[row[0]] = None
						print("val:"+str(row[0])+":"+str(row[1]))
					try:
						self.dtoMin.__dict__[row[0]] = float(row[2])
					except:
						self.dtoMin.__dict__[row[0]] = None
						print("min:"+str(row[0])+":"+str(row[2]))
					try:
						self.dtoMax.__dict__[row[0]] = float(row[3])
					except:
						self.dtoMax.__dict__[row[0]] = None
						print("max:"+str(row[0])+":"+str(row[3]))

				elif self.sanitizer.defaultValues[row[0]][1] is DataTypes.TEXT:
						self.dto.__dict__[row[0]] = str(row[1])
			
	def findFilePath(self):
		# Créer le fichier BalanceTankem.csv s'il n'existe pas
		desktopPath = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop') 
		if os.path.exists(desktopPath + "/BalanceTankem.csv"):
			balance_exist = True
		else:
			balance_exist = False
		open(desktopPath + "/BalanceTankem.csv", "a").close()

		# Dialog pour le chemin
		self.path = tkFileDialog.askopenfilename(initialdir = desktopPath, title = "Sélectionner un fichier csv pour y enregistrer les données", initialfile="BalanceTankem.csv", filetypes=[("CSV files","*.csv"), ("All files", "*")])

		# Détruire le fichier BalanceTankem.csv s'il est inutile
		if self.path is not (desktopPath + "/BalanceTankem.csv") and not balance_exist:
			os.remove(desktopPath + "/BalanceTankem.csv")
	
	def openExcel(self):
		os.system("start excel.exe " + self.path)
