# coding: utf-8
import cx_Oracle
import os,sys,inspect
import traceback
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import id_oracle_account
from levelEditorDTO import LevelEditorDTO
from mapDTO import MapDTO
from tuileDTO import TuileDTO

class LevelEditorDAOoracle():
	def __init__(self):
		self.oracleConnection =None
		self.oracleCursor = None
		self.dto = LevelEditorDTO()

	def getConnection(self):
		try:	
			self.oracleConnection = cx_Oracle.connect(id_oracle_account.id, id_oracle_account.password, 'delta/decinfo.edu')
			self.oracleCursor = self.oracleConnection.cursor()
			return True
		except:
			return False


	def closeConnection(self):
		self.oracleCursor.close()
		self.oracleConnection.close()


	def selectMapInfo(self):
		query = "SELECT id, nom FROM Niveau WHERE status_id = (SELECT id FROM NomStatus WHERE nom = 'actif') ORDER BY nom"
		self.oracleCursor.execute(query)
		query_results = self.oracleCursor.fetchall()
		if query_results:
			for row in query_results:
				self.dto.mapsInfo.append((row[0], row[1]))


	def selectMap(self, mapId):
		query = "SELECT balance_id, delai_min, delai_max, nb_tuiles_x, nb_tuiles_y FROM Niveau where id = '" + str(mapId) + "'"
		self.oracleCursor.execute(query)
		query_results = self.oracleCursor.fetchall()
		self.dto.selectedMap = MapDTO(mapId, query_results[0][0], query_results[0][1], query_results[0][2], query_results[0][3], query_results[0][4])
		self.selectTuiles(mapId)

	def selectTuiles(self, mapId):
		query = "SELECT Tuile.id, NomTuile.nom, Tuile.x, Tuile.y, Tuile.joueur, Tuile.arbre FROM Tuile INNER JOIN NomTuile ON Tuile.nom_id = NomTuile.id WHERE Tuile.niveau_id = '" + str(mapId) + "'"
		self.oracleCursor.execute(query)
		query_results = self.oracleCursor.fetchall()
		if query_results:
			for row in query_results:
				self.dto.selectedMap.tuiles.append(TuileDTO(row[0], row[1], row[2], row[3], row[4], row[5]))
				if row[4]==1:
					self.dto.selectedMap.joueur1PosX = row[2]
					self.dto.selectedMap.joueur1PosY = row[3]
				elif row[4]==2:
					self.dto.selectedMap.joueur2PosX = row[2]
					self.dto.selectedMap.joueur2PosY = row[3]


# dao = LevelEditorDAOoracle()
# dao.getConnection()
# dao.selectMap(2)
# print(dao.dto.selectedMap.joueur1PosX)
# print(dao.dto.selectedMap.joueur1PosY)
# print(dao.dto.selectedMap.joueur2PosX)
# print(dao.dto.selectedMap.joueur2PosY)