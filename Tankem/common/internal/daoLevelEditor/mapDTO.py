
class MapDTO():
	def __init__(self, oracleId, balanceId, delaiMin, delaiMax, nbTuilesX, nbTuilesY):
		self.id = oracleId
		self.tuiles = []
		self.balanceId = balanceId
		self.delaiMin = delaiMin
		self.delaiMax = delaiMax
		self.nbTuilesX = nbTuilesX
		self.nbTuilesY = nbTuilesY
		self.joueur1PosX = None
		self.joueur1PosY = None
		self.joueur2PosX = None
		self.joueur2PosY = None